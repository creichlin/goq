package ast

type Comma struct {
	Left  Element
	Right Element
}

func (c *Comma) String() string {
	return "comma:\n" + printParam("left", c.Left) + "\n" + printParam("right", c.Right)
}


type BinaryOperation struct {
	Operation string
	Left  Element
	Right Element
}

func (c *BinaryOperation) String() string {
	return "[" + c.Operation + "]:\n" + printParam("left", c.Left) + "\n" + printParam("right", c.Right)
}
