package ast

import (
	"strings"
)

type Element interface {
	String() string
	// Eval(data interface{}) (interface{}, error)
}

type Expressions struct {
	Filters []Element
}

func (f *Expressions) String() string {
	ret := "(\n"
	for _, f := range f.Filters {
		ret += indent(f.String()) + "\n"
	}
	return ret + ")"
}

type Block struct {
	Element Element
}

func (f *Block) String() string {
	ret := "(((\n"
	ret += indent(f.Element.String()) + "\n"
	return ret + ")))"
}

func indent(i string) string {
	parts := []string{}
	for _, l := range strings.Split(i, "\n") {
		parts = append(parts, "  "+l)
	}
	return strings.Join(parts, "\n")
}

func printParam(name string, value Element) string {
	v := value.String()
	if strings.Contains(v, "\n") {
		return indent(name + ":\n" + indent(v))
	} else {
		return indent(name + ": " + v)
	}
}

type NoError struct {
	Element Element
}

func (a *NoError) String() string {
	return "eatError:\n" +
		printParam("from", a.Element)
}

type Def struct {
	Name   string
	Code   Element
	Params []*Param
}

func (d *Def) String() string {
	params := []string{}
	for _, p := range d.Params {
		params = append(params, p.String())
	}
	return "def " + d.Name + "(" + strings.Join(params, ", ") + "):\n" +
		printParam("body", d.Code)
}

type Param struct {
	Name string
}

func (p *Param) String() string {
	return p.Name
}
