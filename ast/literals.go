package ast

import "fmt"

type String struct {
	Value string
}

func NewEscapedString(str string) *String {
	return NewString(str[1 : len(str)-1])
}

func NewString(str string) *String {
	return &String{
		Value: str,
	}
}

func (s *String) String() string {
	return "\"" + s.Value + "\""
}

type Number struct {
	Value float64
}

func (s *Number) String() string {
	return fmt.Sprintf("%v", s.Value)
}

type Bool bool

func (s Bool) String() string {
	if s {
		return "true"
	}
	return "false"
}

type Null struct {
}

func (s *Null) String() string {
	return "NULL"
}

var NULL = &Null{}

type Array struct {
	Elements Element
}

func (a *Array) String() string {
	return "[]:\n" + printParam("elements", a.Elements)
}

type Object struct {
	Entries []*Entry
}

type Entry struct {
	Key   Element
	Value Element
}

func (a *Object) String() string {
	return "{}:\n"
}
