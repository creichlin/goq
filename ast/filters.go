package ast

import "strconv"

type ID struct {
}

func (i *ID) String() string {
	return "id:"
}

type Slice struct {
	From, To, Step Element
}

func (a *Slice) String() string {
	return "arraySlice:\n" +
		printParam("from", a.From) + "\n" +
		printParam("to", a.To)
}

type Index struct {
	Name Element
}

func (a *Index) String() string {
	return "index:\n" + printParam("key", a.Name)
}

type Any struct {
	Of Element
}

func (a *Any) String() string {
	return "any:\n" + printParam("of", a.Of)
}

type Recurse struct {
	Filter    Element
	Condition Element
}

func (d *Recurse) String() string {
	return "recurse:\n" + printParam("filter", d.Filter) + "\n" + printParam("condition", d.Condition)
}

type Function struct {
	Name       string
	Parameters []Element
}

func (f *Function) String() string {
	s := f.Name + "():"

	for i, param := range f.Parameters {
		s += "\n" + printParam(strconv.Itoa(i), param)
	}

	return s
}
