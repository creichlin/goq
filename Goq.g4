grammar Goq;


expression
    : '(' expression ')'                                              # group
    | 'null'                                                          # null
    | basicExpression+                                                # basicExpressions
    | 'def' NAME ('(' (param (';' param)*)? ')')? ':' expression ';'  # def
    | expression '[]'                                                 # any
    | expression '?'                                                  # noError
    | BOOL                                                            # bool
    | INT                                                             # integer
    | STRING                                                          # string
    | NAME ('(' (expression (';' expression)*)? ')')?                 # function
    | '[' expression ']'                                              # array
    | '{' (objectEntry (',' objectEntry)*)? '}'                       # object
    | expression op=('*'|'/'|'%') expression                          # mulOperations
    | expression op=('+' | '-') expression                            # addOperations
    | expression op=('<' | '<=' | '>' | '>=') expression              # lgOperators
    | expression op=('and' | 'or') expression                         # boolOperators
    | expression op=('==' | '!=') expression                          # eqOperators
    | expression ',' expression                                       # comma
    | expression ('|'? expression)+                                   # expressions
    ;

basicExpression
    : '.' NAME                                                        # objectIdentifier
    | '.' '[' expression ']'                                          # index
    | '.' '[' optionalExpression ':' optionalExpression ']'           # slice
    | '..'                                                            # recurseDescent
    | '.'                                                             # identity
    ;

optionalExpression
    : expression?
    ;

objectEntry
    : key (':' expression) ?
    ;

key
    : NAME
    | STRING
    | '(' expression ')'
    ;

param
    : NAME
    ;

BOOL
    : 'true'
    | 'false'
    ;

NAME
    : [a-zA-Z_][a-zA-Z0-9_]*
    ;

STRING
    : '"' (ESC | ~ ["\\])* '"'
    ;

fragment ESC
    : '\\' (["\\/bfnrt] | UNICODE)
    ;

fragment UNICODE
    : 'u' HEX HEX HEX HEX
    ;

fragment HEX
    : [0-9a-fA-F]
    ;

INT
    : '-'? UINT
    ;

NUMBER
    : '-'? UINT '.' [0-9] + EXP? | '-'? UINT EXP | '-'? UINT
    ;

fragment UINT
    : '0' | [1-9] [0-9]*
    ;

fragment EXP
    : [Ee] [+\-]? UINT
    ;

// WS represents a whitespace, which is ignored entirely by skip.
WS
    : [ \t\r\n]+ -> skip
;