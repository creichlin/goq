package goq

import (
	"fmt"
	"testing"
)

func TestAST(t *testing.T) {
	ast, errs := ReadFromString("..foo.bar.[](.[].[7]).[:5].[2:-6].[:].[2 + 4]")
	fmt.Println(errs)
	fmt.Println(ast)
}

func TestAST2(t *testing.T) {
	ast, errs := ReadFromString("2 + 4 * 4")
	fmt.Println(errs)
	fmt.Println(ast)
}

func TestFailing(t *testing.T) {
	ast, errs := ReadFromString(".[] | tostring")
	fmt.Println("parse error ", errs)
	fmt.Println("ast\n", ast)

	result, err := Evaluate(ast, map[string]interface{}{"aa": []interface{}{"aaa", 3}})
	fmt.Println("eval-error ", err)
	fmt.Println("result     ", result.Items)

}
