package evaluate

import (
	"fmt"
	"math"
	"reflect"
	"strings"
)

func init() {
	// Addition +
	defineBinaryOperation(Float64, "+", Float64, func(l, r interface{}) (interface{}, error) {
		return l.(float64) + r.(float64), nil
	})
	defineBinaryOperation(Null, "+", Float64, func(l, r interface{}) (interface{}, error) {
		return r, nil
	})
	defineBinaryOperation(Float64, "+", Null, func(l, r interface{}) (interface{}, error) {
		return l, nil
	})
	defineBinaryOperation(Slice, "+", Slice, func(l, r interface{}) (interface{}, error) {
		ns := []interface{}{}
		for _, v := range l.([]interface{}) {
			ns = append(ns, v)
		}
		for _, v := range r.([]interface{}) {
			ns = append(ns, v)
		}
		return ns, nil
	})
	defineBinaryOperation(Null, "+", Slice, func(l, r interface{}) (interface{}, error) {
		return r, nil
	})
	defineBinaryOperation(Slice, "+", Null, func(l, r interface{}) (interface{}, error) {
		return l, nil
	})
	defineBinaryOperation(Map, "+", Map, func(l, r interface{}) (interface{}, error) {
		nm := map[string]interface{}{}

		for k, v := range l.(map[string]interface{}) {
			nm[k] = v
		}
		for k, v := range r.(map[string]interface{}) {
			nm[k] = v
		}

		return nm, nil
	})
	defineBinaryOperation(Null, "+", Map, func(l, r interface{}) (interface{}, error) {
		return r, nil
	})
	defineBinaryOperation(Map, "+", Null, func(l, r interface{}) (interface{}, error) {
		return l, nil
	})
	// Subtraction -
	defineBinaryOperation(Float64, "-", Float64, func(l, r interface{}) (interface{}, error) {
		return l.(float64) - r.(float64), nil
	})
	defineBinaryOperation(Slice, "-", Slice, func(l, r interface{}) (interface{}, error) {
		ns := []interface{}{}
		for _, vl := range l.([]interface{}) {
			found := false
			for _, vr := range r.([]interface{}) {
				if reflect.DeepEqual(vr, vl) {
					found = true
					break
				}
			}
			if !found {
				ns = append(ns, vl)
			}
		}
		return ns, nil
	})
	// Multiplication *
	defineBinaryOperation(Float64, "*", Float64, func(l, r interface{}) (interface{}, error) {
		return l.(float64) * r.(float64), nil
	})
	defineBinaryOperation(Map, "*", Map, func(l, r interface{}) (interface{}, error) {
		return merge(l, r), nil
	})
	// Division /
	defineBinaryOperation(Float64, "/", Float64, func(l, r interface{}) (interface{}, error) {
		if r.(float64) == 0 {
			return nil, fmt.Errorf("Division by zero")
		}
		return l.(float64) / r.(float64), nil
	})
	defineBinaryOperation(String, "/", String, func(l, r interface{}) (interface{}, error) {
		result := []interface{}{}
		for _, part := range strings.Split(l.(string), r.(string)) {
			result = append(result, part)
		}
		return result, nil
	})
	// Modulo %
	defineBinaryOperation(Float64, "%", Float64, func(l, r interface{}) (interface{}, error) {
		return math.Mod(l.(float64), r.(float64)), nil
	})

	// equals ==
	defineBinaryOperation(Float64, "==", Float64, func(l, r interface{}) (interface{}, error) {
		return l.(float64) == r.(float64), nil
	})
	defineBinaryOperation(String, "==", Float64, func(l, r interface{}) (interface{}, error) {
		return false, nil
	})
	defineBinaryOperation(Float64, "==", String, func(l, r interface{}) (interface{}, error) {
		return false, nil
	})

	// smaller <
	defineBinaryOperation(Float64, "<", Float64, func(l, r interface{}) (interface{}, error) {
		return l.(float64) < r.(float64), nil
	})
	defineBinaryOperation(Float64, "<=", Float64, func(l, r interface{}) (interface{}, error) {
		return l.(float64) <= r.(float64), nil
	})
	// greater >
	defineBinaryOperation(Float64, ">", Float64, func(l, r interface{}) (interface{}, error) {
		return l.(float64) > r.(float64), nil
	})
	defineBinaryOperation(Float64, ">=", Float64, func(l, r interface{}) (interface{}, error) {
		return l.(float64) >= r.(float64), nil
	})
	// and
	defineBinaryOperation(Any, "and", Any, func(l, r interface{}) (interface{}, error) {
		return asBool(l) && asBool(r), nil
	})
	// or
	defineBinaryOperation(Any, "or", Any, func(l, r interface{}) (interface{}, error) {
		return asBool(l) || asBool(r), nil
	})
}

func merge(l, r interface{}) interface{} {
	if l == nil {
		return r
	}
	if r == nil {
		return l
	}

	if lmap, lismap := l.(map[string]interface{}); lismap {
		if rmap, rismap := r.(map[string]interface{}); rismap {
			// we have two maps, merge them
			nmap := map[string]interface{}{}

			allkeys := map[string]interface{}{}
			for k := range lmap {
				allkeys[k] = true
			}
			for k := range rmap {
				allkeys[k] = true
			}

			for k := range allkeys {
				nmap[k] = merge(lmap[k], rmap[k])
			}
			return nmap
		}
	}
	return r
}
