package evaluate

import (
	"fmt"
	"reflect"

	"github.com/creichlin/gutil"
	"gitlab.com/creichlin/goq/ast"
)

var binopDefinitions = map[reflect.Kind]map[string]map[reflect.Kind]func(l, r interface{}) (interface{}, error){}

type binOpFunc func(interface{}, interface{}) (interface{}, error)

// nil and boolean false is false, everything else is true
func asBool(i interface{}) bool {
	if i == nil {
		return false
	}
	if b, is := i.(bool); is {
		return b
	}
	return true
}

func (e *Eval) BinaryOperation(element *ast.BinaryOperation, i *Data) (*Data, error) {
	left, err := e.Eval(element.Left, i)
	if err != nil {
		return NewEmptyData(), err
	}

	right, err := e.Eval(element.Right, i)
	if err != nil {
		return NewEmptyData(), err
	}

	result := &Data{}
	errs := gutil.ErrorCollector{}
	for cl := 0; cl < len(left.Items); cl++ {
		for cr := 0; cr < len(right.Items); cr++ {
			ret, err := callBinaryOperation(element, left.Items[cl], right.Items[cr])
			if err != nil {
				errs.Add(err)
			} else {
				result.Items = append(result.Items, ret)
			}
		}
	}
	return result, errs.ThisOrNil()
}

func callBinaryOperation(element *ast.BinaryOperation, le, re interface{}) (interface{}, error) {
	l := getTypeOf(le)
	r := getTypeOf(re)

	op := getBinOpFor(l, element.Operation, r)
	if op == nil {
		op = getBinOpFor(Any, element.Operation, r)
	}
	if op == nil {
		op = getBinOpFor(l, element.Operation, Any)
	}
	if op == nil {
		op = getBinOpFor(Any, element.Operation, Any)
	}
	if op == nil {
		return nil, fmt.Errorf("Can not %v %T and %T", element.Operation, le, re)
	}
	return op(le, re)
}

func getBinOpFor(l reflect.Kind, op string, r reflect.Kind) binOpFunc {
	if ops, has := binopDefinitions[l]; has {
		if rks, has := ops[op]; has {
			if f, has := rks[r]; has {
				return f
			}
		}
	}
	return nil
}

func defineBinaryOperation(l reflect.Kind, o string, r reflect.Kind, f func(interface{}, interface{}) (interface{}, error)) {
	if _, has := binopDefinitions[l]; !has {
		binopDefinitions[l] = map[string]map[reflect.Kind]func(l, r interface{}) (interface{}, error){}
	}
	ops := binopDefinitions[l]
	if _, has := ops[o]; !has {
		ops[o] = map[reflect.Kind]func(l, r interface{}) (interface{}, error){}
	}
	right := ops[o]
	right[r] = f
}
