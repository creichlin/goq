package evaluate

import "reflect"

// types are of type reflect.Kind
// we add some more custom ones like Null
const (
	Slice   = reflect.Slice
	Float64 = reflect.Float64
	String  = reflect.String
	Map     = reflect.Map
	Bool    = reflect.Bool
	Null    = reflect.Kind(1024)
	Any     = reflect.Kind(1024 + 1)
)

func getTypeOf(i interface{}) reflect.Kind {
	if i == nil {
		return Null
	}
	return reflect.TypeOf(i).Kind()
}
