package evaluate

import (
	"encoding/json"
	"fmt"
	"math"
	"reflect"
	"sort"
	"strconv"

	"gitlab.com/creichlin/goq/ast"
)

func init() {
	defineFilter(Slice, "length", func(_ *Eval, _ *ast.Function, _ *Data, i interface{}) (interface{}, error) {
		return float64(len(i.([]interface{}))), nil
	})
	defineFilter(Map, "length", func(_ *Eval, _ *ast.Function, _ *Data, i interface{}) (interface{}, error) {
		return float64(len(i.(map[string]interface{}))), nil
	})
	defineFilter(String, "length", func(_ *Eval, _ *ast.Function, _ *Data, i interface{}) (interface{}, error) {
		return float64(len(i.(string))), nil
	})
	defineFilter(Null, "length", func(_ *Eval, _ *ast.Function, _ *Data, i interface{}) (interface{}, error) {
		return float64(0), nil
	})
	defineFilter(String, "utf8bytelength", func(_ *Eval, _ *ast.Function, _ *Data, i interface{}) (interface{}, error) {
		return float64(len([]byte(i.(string)))), nil
	})
	defineFilter(Map, "keys", func(_ *Eval, _ *ast.Function, _ *Data, i interface{}) (interface{}, error) {
		keys := []string{}

		for k := range i.(map[string]interface{}) {
			keys = append(keys, k)
		}

		sort.Strings(keys)
		keysi := []interface{}{}

		for _, v := range keys {
			keysi = append(keysi, v)
		}

		return keysi, nil
	})
	defineFilter(Slice, "keys", func(_ *Eval, _ *ast.Function, _ *Data, i interface{}) (interface{}, error) {
		keys := []interface{}{}

		for ix := range i.([]interface{}) {
			keys = append(keys, float64(ix))
		}

		return keys, nil
	})
	defineFilter(Slice, "add", func(_ *Eval, _ *ast.Function, _ *Data, i interface{}) (interface{}, error) {
		k := reflect.Invalid
		for _, ix := range i.([]interface{}) {
			if ix == nil {
				continue
			}
			if k == reflect.Invalid {
				k = getTypeOf(ix)
			} else {
				if k != getTypeOf(ix) {
					return nil, fmt.Errorf("can not add different types")
				}
			}
		}

		if k == String {
			result := ""
			for _, ix := range i.([]interface{}) {
				result += ix.(string)
			}
			return result, nil
		}
		if k == Float64 {
			result := float64(0)
			for _, ix := range i.([]interface{}) {
				result += ix.(float64)
			}
			return result, nil
		}
		return nil, nil
	})

	defineFilter(Slice, "reverse", func(_ *Eval, _ *ast.Function, _ *Data, i interface{}) (interface{}, error) {
		res := []interface{}{}
		ix := i.([]interface{})
		for c := len(ix) - 1; c > -1; c-- {
			res = append(res, ix[c])
		}
		return res, nil
	})

	defineFilter(Float64, "floor", func(_ *Eval, _ *ast.Function, _ *Data, i interface{}) (interface{}, error) {
		return math.Floor(i.(float64)), nil
	})
	defineFilter(Float64, "sqrt", func(_ *Eval, _ *ast.Function, _ *Data, i interface{}) (interface{}, error) {
		return math.Sqrt(i.(float64)), nil
	})

	defineFilter(String, "tonumber", func(_ *Eval, _ *ast.Function, _ *Data, i interface{}) (interface{}, error) {
		return strconv.ParseFloat(i.(string), 64)
	})

	defineFilter(Float64, "tonumber", func(_ *Eval, _ *ast.Function, _ *Data, i interface{}) (interface{}, error) {
		return i, nil
	})

	defineFilter(Float64, "tostring", func(_ *Eval, _ *ast.Function, _ *Data, i interface{}) (interface{}, error) {
		d, err := json.Marshal(i)
		return string(d), err
	})

	defineFilter(String, "tostring", func(_ *Eval, _ *ast.Function, _ *Data, i interface{}) (interface{}, error) {
		return i, nil
	})

	defineFilter(Map, "tostring", func(_ *Eval, _ *ast.Function, _ *Data, i interface{}) (interface{}, error) {
		d, err := json.Marshal(i)
		return string(d), err
	})

	defineFilter(Slice, "tostring", func(_ *Eval, _ *ast.Function, _ *Data, i interface{}) (interface{}, error) {
		d, err := json.Marshal(i)
		return string(d), err
	})

	defineFilter(Slice, "unique", func(_ *Eval, _ *ast.Function, _ *Data, i interface{}) (interface{}, error) {
		u := map[interface{}]bool{}
		result := []interface{}{}
		for _, v := range i.([]interface{}) {
			if _, has := u[v]; !has {
				u[v] = true
				result = append(result, v)
			}
		}

		return result, nil
	})
	//defineFilter(Slice, "map", func(eval *Eval, f *ast.Function, _ *Data, ie interface{}) (interface{}, error) {
	//	res := []interface{}{}
	//
	//	for _, v := range ie.([]interface{}) {
	//		r, err := eval.Eval(f.Parameters[0], NewSingleData(v))
	//		if err != nil {
	//			return nil, err
	//		}
	//		res = append(res, r.Items[0])
	//	}
	//
	//	return res, nil
	//})
	defineFilter(Map, "map_values", func(eval *Eval, f *ast.Function, _ *Data, ie interface{}) (interface{}, error) {
		res := map[string]interface{}{}

		for k, v := range ie.(map[string]interface{}) {
			r, err := eval.Eval(f.Parameters[0], NewSingleData(v))
			if err != nil {
				return nil, err
			}
			res[k] = r.Items[0]
		}

		return res, nil
	})

	defineFilter(Map, "has", func(eval *Eval, f *ast.Function, i *Data, ie interface{}) (interface{}, error) {
		r, err := eval.Eval(f.Parameters[0], i)

		if err != nil {
			return nil, err
		}

		if _, is := r.Items[0].(string); !is {
			return nil, fmt.Errorf("has(...) Cannot check whether object has a number key")
		}

		for k := range ie.(map[string]interface{}) {
			if k == r.Items[0] {
				return true, nil
			}
		}

		return false, nil
	})
	defineFilter(Slice, "has", func(eval *Eval, f *ast.Function, i *Data, ie interface{}) (interface{}, error) {
		r, err := eval.Eval(f.Parameters[0], i)

		if err != nil {
			return nil, err
		}
		if i, is := r.Items[0].(float64); is {
			i := int(i)
			if i >= 0 && i < len(ie.([]interface{})) {
				return true, nil
			}

		} else {
			return nil, fmt.Errorf("has(...) Array must have a number key")
		}

		return false, nil
	})
	defineFilter(Slice, "any", func(eval *Eval, f *ast.Function, i *Data, ie interface{}) (interface{}, error) {
		for _, v := range ie.([]interface{}) {
			if asBool(v) {
				return true, nil
			}
		}
		return false, nil
	})
	defineFilter(Slice, "all", func(eval *Eval, f *ast.Function, i *Data, ie interface{}) (interface{}, error) {
		for _, v := range ie.([]interface{}) {
			if !asBool(v) {
				return false, nil
			}
		}
		return true, nil
	})
	defineFilter(Slice, "type", func(eval *Eval, f *ast.Function, i *Data, ie interface{}) (interface{}, error) {
		return "array", nil
	})
	defineFilter(Bool, "type", func(eval *Eval, f *ast.Function, i *Data, ie interface{}) (interface{}, error) {
		return "boolean", nil
	})
	defineFilter(Float64, "type", func(eval *Eval, f *ast.Function, i *Data, ie interface{}) (interface{}, error) {
		return "number", nil
	})
	defineFilter(String, "type", func(eval *Eval, f *ast.Function, i *Data, ie interface{}) (interface{}, error) {
		return "string", nil
	})
	defineFilter(Map, "type", func(eval *Eval, f *ast.Function, i *Data, ie interface{}) (interface{}, error) {
		return "object", nil
	})
	defineFilter(Null, "type", func(eval *Eval, f *ast.Function, i *Data, ie interface{}) (interface{}, error) {
		return "null", nil
	})
}
