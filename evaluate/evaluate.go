package evaluate

import (
	"fmt"
	"math"

	"github.com/creichlin/gutil"
	"gitlab.com/creichlin/goq/ast"
	"gopkg.in/yaml.v2"
)

func NewEval() *Eval {
	return &Eval{
		contextStack: []*Context{{Def: map[string]*ast.Def{}}},
	}
}

type Data struct {
	Items []interface{}
}

func (d *Data) Append(i interface{}) {
	d.Items = append(d.Items, i)
}

func NewEmptyData() *Data {
	return &Data{Items: []interface{}{}}
}

func NewSingleData(vals ...interface{}) *Data {
	return &Data{Items: vals}
}

func (d *Data) YAML() string {
	if d == nil {
		return "EMPTY"
	}
	y, err := yaml.Marshal(d.Items)
	if err != nil {
		panic(err)
	}
	return string(y)
}

type Eval struct {
	contextStack []*Context
}

type Context struct {
	parent *Context
	Def    map[string]*ast.Def
}

func (c *Context) setDef(name string, params int, def *ast.Def) {
	c.Def[fmt.Sprintf("%v:%v", name, params)] = def
}

func (c *Context) FindDef(name string, params int) *ast.Def {
	if def, has := c.Def[fmt.Sprintf("%v:%v", name, params)]; has {
		return def
	}
	if c.parent != nil {
		return c.parent.FindDef(name, params)
	}
	return nil
}

func (e *Eval) pushContext() {
	e.contextStack = append(e.contextStack, &Context{
		parent: e.Context(),
		Def:    map[string]*ast.Def{},
	})
}

func (e *Eval) popContext() {
	e.contextStack = e.contextStack[:len(e.contextStack)-1]
}

func (e *Eval) Context() *Context {
	return e.contextStack[len(e.contextStack)-1]
}

func (e *Eval) Eval(element ast.Element, i *Data) (*Data, error) {
	if element == nil {
		return nil, nil
	}

	switch t := element.(type) {
	case *ast.ID:
		return i, nil

	case *ast.NoError:
		ret, _ := e.Eval(t.Element, i)
		return ret, nil

	case *ast.Null:
		return NewSingleData(nil), nil

	case ast.Bool:
		return NewSingleData(bool(t)), nil

	case *ast.String:
		return NewSingleData(t.Value), nil

	case *ast.Number:
		return NewSingleData(t.Value), nil

	case *ast.Index:
		return e.Index(t, i)

	case *ast.Expressions:
		return e.Expressions(t, i)

	case *ast.Block:
		return e.Block(t, i)

	case *ast.Def:
		return e.Def(t, i)

	case *ast.Any:
		return e.Any(t, i)

	case *ast.Recurse:
		return e.Recurse(t, i)

	case *ast.Slice:
		return e.Slice(t, i)

	case *ast.Array:
		return e.Array(t, i)

	case *ast.Object:
		return e.Object(t, i)

	case *ast.BinaryOperation:
		return e.BinaryOperation(t, i)

	case *ast.Function:
		return e.Function(t, i)

	case *ast.Comma:
		return e.Comma(t, i)

	default:
		return nil, fmt.Errorf("%T not implemented", element)
	}
}

func (e *Eval) Function(element *ast.Function, i *Data) (*Data, error) {
	// here we check the defs:
	def := e.Context().FindDef(element.Name, len(element.Parameters))
	if def != nil {
		e.pushContext()

		for pnum, param := range def.Params {
			paramVal := element.Parameters[pnum]
			e.Context().setDef(param.Name, 0, &ast.Def{Name: param.Name, Code: paramVal})
		}

		res, err := e.Eval(def.Code, i)
		e.popContext()
		return res, err
	}

	result := &Data{}
	errs := gutil.ErrorCollector{}
	for c := 0; c < len(i.Items); c++ {
		i, err := callFilter(e, element, i, i.Items[c])
		if err != nil {
			errs.Add(err)
		} else {
			result.Items = append(result.Items, i)
		}
	}
	return result, errs.ThisOrNil()
}

func (e *Eval) String(element *ast.String, i *Data) (*Data, error) {
	return &Data{
		Items: []interface{}{
			element.Value,
		},
	}, nil
}

func (e *Eval) Array(element *ast.Array, i *Data) (*Data, error) {
	arr := []interface{}{}

	res, err := e.Eval(element.Elements, i)
	if err != nil {
		return nil, err
	}
	for _, e := range res.Items {
		arr = append(arr, e)
	}

	return &Data{Items: []interface{}{arr}}, nil
}

func (e *Eval) Object(element *ast.Object, i *Data) (*Data, error) {
	res := []map[string]interface{}{{}}

	// we loop from the last key to the first so it will be consistent with jq behaviour
	for c := len(element.Entries) - 1; c >= 0; c-- {
		entry := element.Entries[c]
		key, err := e.Eval(entry.Key, i)
		if err != nil {
			return nil, err
		}
		val, err := e.Eval(entry.Value, i)
		if err != nil {
			return nil, err
		}

		if strkey, is := key.Items[0].(string); is {
			resNew := []map[string]interface{}{}
			for _, value := range val.Items {
				resn := copy(res)
				for _, resnItem := range resn {
					resnItem[strkey] = value
				}
				resNew = append(resNew, resn...)
			}
			res = resNew
		} else {
			return nil, fmt.Errorf("key in object literal must be string but is %T", key.Items[0])
		}

	}

	d := &Data{}
	for _, r := range res {
		d.Append(r)
	}

	return d, nil
}

// copy will do a shallow copy because the maps values will never be modified
func copy(i []map[string]interface{}) []map[string]interface{} {
	result := []map[string]interface{}{}
	for _, m := range i {
		entry := map[string]interface{}{}
		for k, v := range m {
			entry[k] = v
		}
		result = append(result, entry)
	}
	return result
}

func (e *Eval) Integer(element *ast.Number, i *Data) (*Data, error) {
	return &Data{
		Items: []interface{}{
			float64(element.Value),
		},
	}, nil
}

func (e *Eval) Expressions(element *ast.Expressions, i *Data) (*Data, error) {
	result := i
	var err error
	for _, exp := range element.Filters {
		result, err = e.Eval(exp, result)
		if err != nil {
			return result, err
		}
	}
	return result, nil
}

func (e *Eval) Block(element *ast.Block, i *Data) (*Data, error) {
	e.pushContext()
	result, err := e.Eval(element.Element, i)
	e.popContext()

	return result, err
}

func (e *Eval) Def(def *ast.Def, i *Data) (*Data, error) {
	e.Context().setDef(def.Name, len(def.Params), def)
	return i, nil
}

func (e *Eval) Comma(element *ast.Comma, i *Data) (*Data, error) {
	result := &Data{}

	lr, err := e.Eval(element.Left, i)
	if err != nil {
		return nil, err
	}
	for _, i := range lr.Items {
		result.Append(i)
	}

	rr, err := e.Eval(element.Right, i)
	if err != nil {
		return nil, err
	}
	for _, i := range rr.Items {
		result.Append(i)
	}

	return result, nil
}

func (e *Eval) Slice(element *ast.Slice, input *Data) (*Data, error) {
	getInt := func(key ast.Element) (float64, error) {
		val, err := e.Eval(key, input)
		if err != nil {
			return math.NaN(), err
		}

		if len(val.Items) != 1 {
			return math.NaN(), fmt.Errorf("slice params must be single values")

		}
		v := val.Items[0]

		if v == nil {
			return math.NaN(), nil
		}

		if vi, is := v.(float64); is {
			return vi, nil
		} else {
			return math.NaN(), fmt.Errorf("Slices params must be of type int")
		}
	}

	from, err := getInt(element.From)
	if err != nil {
		return nil, err
	}
	to, err := getInt(element.To)
	if err != nil {
		return nil, err
	}
	step, err := getInt(element.Step)
	if err != nil {
		return nil, err
	}
	if math.IsNaN(from) {
		from = 0
	}
	if math.IsNaN(step) {
		step = 0
	}

	result := &Data{}

	for _, item := range input.Items {
		if list, is := item.([]interface{}); is {
			from := round(from)
			tox := len(list)
			if !math.IsNaN(to) {
				tox = round(to)
			}
			if from < 0 {
				from = len(list) + from
			}
			if tox < 0 {
				tox = len(list) + tox
			}
			result.Append(list[from:tox])
		} else if str, is := item.(string); is {
			from := round(from)
			tox := len(str)
			if !math.IsNaN(to) {
				tox = round(to)
			}
			if from < 0 {
				from = len(str) + from
			}
			if tox < 0 {
				tox = len(str) + tox
			}
			result.Append(str[from:tox])
		} else {
			return nil, fmt.Errorf("Slice can only work on lists")
		}
	}

	return result, nil
}

func (e *Eval) Any(element *ast.Any, i *Data) (*Data, error) {
	result := &Data{}
	i2, err := e.Eval(element.Of, i)
	if err != nil {
		return nil, err
	}

	for _, d := range i2.Items {
		if list, is := d.([]interface{}); is {
			for _, li := range list {
				result.Append(li)
			}
		} else if m, is := d.(map[string]interface{}); is {
			for _, li := range m {
				result.Append(li)
			}
		} else {
			// result.Append(nil) this should be readded when recurse condition is implemented
			err = fmt.Errorf("Can not call any on %T", d)
		}
	}
	return result, err
}

func (e *Eval) Recurse(element *ast.Recurse, i *Data) (*Data, error) {
	all := &Data{}
	current := i
	for _, e := range current.Items {
		all.Append(e)
	}

	for len(current.Items) > 0 {
		next, err := e.Eval(element.Filter, current)
		if err != nil {
			return nil, err
		}

		// now we need select to filter out, not implemented yet though

		for _, e := range next.Items {
			all.Append(e)
		}
		current = next
	}

	fmt.Println("result", all.Items)

	return all, nil
}

func (e *Eval) Index(element *ast.Index, input *Data) (*Data, error) {
	key, err := e.Eval(element.Name, input)
	if err != nil {
		return nil, err
	}

	errors := gutil.ErrorCollector{}

	result := &Data{}
	for _, item := range input.Items {

		for _, key := range key.Items {
			if str, is := key.(string); is {
				if m, ism := item.(map[string]interface{}); ism {
					result.Append(m[str])
				} else {
					errors.Add(fmt.Errorf("index: not a map but %T", item))
				}
			} else if float, is := key.(float64); is {
				integer := round(float)
				for _, item := range input.Items {
					if m, ism := item.([]interface{}); ism {
						iii := integer
						if iii < 0 {
							iii = len(m) + iii
						}
						if iii >= len(m) || iii < 0 {
							return &Data{Items: []interface{}{nil}}, nil
						}
						result.Append(m[iii])
					} else {
						errors.Add(fmt.Errorf("index: not a list but %T", item))
					}
				}
			} else {
				errors.Add(fmt.Errorf("index is neither string nor int"))
			}
		}
	}

	return result, errors.ThisOrNil()
}

func round(val float64) int {
	if val < 0 {
		return int(val - 0.5)
	}
	return int(val + 0.5)
}
