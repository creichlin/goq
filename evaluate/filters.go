package evaluate

import (
	"fmt"
	"reflect"

	"gitlab.com/creichlin/goq/ast"
)

type filterCall func(*Eval, *ast.Function, *Data, interface{}) (interface{}, error)

var filterDefinitions = map[reflect.Kind]map[string]filterCall{}

func callFilter(e *Eval, f *ast.Function, i *Data, ie interface{}) (interface{}, error) {
	// first we check all the hardcoded functions names
	if ops, has := filterDefinitions[getTypeOf(ie)]; has {
		if rks, has := ops[f.Name]; has {
			return rks(e, f, i, ie)
		}
	}

	return nil, fmt.Errorf("Can not call function %v with input %T", f.Name, ie)
}

func defineFilter(l reflect.Kind, o string, f filterCall) {
	if _, has := filterDefinitions[l]; !has {
		filterDefinitions[l] = map[string]filterCall{}
	}
	ops := filterDefinitions[l]
	ops[o] = f
}
