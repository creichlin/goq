package goq

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"path"
	"reflect"
	"strings"
	"testing"
)

// testcases json is generated from jq doc with
// [.. | .entries? | select(. != null) | .[] | select(.examples != null) |
// {
//   cases: [.examples[] | {input : .input | tostring, output: [.output[] | tostring], program: .program}],
//   title: .title
// }]

type TestCase struct {
	Input   string   `json:"input"`
	Program string   `json:"program"`
	Output  []string `json:"output"`
}

type TestPackage struct {
	Title string     `json:"title"`
	Cases []TestCase `json:"cases""`
}

func TestJQDoc(t *testing.T) {
	testCasesInFile(t, "jqdoc.json")
}

func TestOperations(t *testing.T) {
	testCasesInFile(t, "operations.json")
}

func TestErrors(t *testing.T) {
	testCasesInFile(t, "errors.json")
}

func TestFunctions(t *testing.T) {
	testCasesInFile(t, "functions.json")
}

func TestDefs(t *testing.T) {
	testCasesInFile(t, "defs.json")
}

func TestPrecedence(t *testing.T) {
	testCasesInFile(t, "precedence.json")
}

func testCasesInFile(t *testing.T, file string) {
	data, err := ioutil.ReadFile(path.Join("tests", file))
	if err != nil {
		t.Error(err)
	}

	packages := []*TestPackage{}

	err = json.Unmarshal(data, &packages)
	if err != nil {
		t.Error(err)
	}

	for i, pkg := range packages {
		fmt.Println(i)
		for index, tc := range pkg.Cases {
			t.Run(fmt.Sprintf("%v:%v", pkg.Title, index), func(subTest *testing.T) {
				runDocTest(subTest, tc)
			})
		}
	}
}

func runDocTest(t *testing.T, tc TestCase) {
	input := fmt.Sprint(tc.Input)
	var inputData interface{}
	json.Unmarshal([]byte(input), &inputData)

	outputData := []interface{}{}

	for _, ops := range tc.Output {
		var opsData interface{}
		json.Unmarshal([]byte(ops), &opsData)
		outputData = append(outputData, opsData)
	}

	fmt.Println("program", tc.Program)

	ast, err := ReadFromString(tc.Program)
	if err != nil {
		fmt.Println(err)
		t.Fail()
		return
	}

	result, err := Evaluate(ast, inputData)
	if err != nil {
		fmt.Println("data", input)
		fmt.Println("expected", tc.Output)
		fmt.Println("ast", ast)
		fmt.Println(err)
		t.Fail()
		return
	}

	results := []interface{}{}
	jsonResults := []string{}

	for _, i := range result.Items {
		results = append(results, i)
		js, _ := json.Marshal(i)

		jsonResults = append(jsonResults, string(js))
	}

	if !reflect.DeepEqual(outputData, results) {
		fmt.Println("data", input)
		fmt.Println("ast", ast)
		fmt.Println("expected", ">"+strings.Join(tc.Output, "\n")+"<")
		fmt.Println("actual  ", ">"+strings.Join(jsonResults, "\n")+"<")
		t.Fail()
	}
}
