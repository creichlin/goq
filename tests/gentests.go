package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os/exec"
	"path"
	"runtime"
)

type Tests struct {
	Title string `json:"title"`
	Cases []Case `json:"cases""`
}

type Case struct {
	Input   interface{} `json:"input"`
	Program string      `json:"program"`
}

type TestsOut struct {
	Title string    `json:"title"`
	Cases []CaseOut `json:"cases""`
}

type CaseOut struct {
	Input   string   `json:"input"`
	Program string   `json:"program"`
	Output  []string `json:"output""`
	Error   string   `json:"error""`
}

func main() {
	names := []string{
		"errors.in.json",
		"precedence.in.json",
		"operations.in.json",
		"functions.in.json",
		"defs.in.json",
	}

	for _, name := range names {
		_, filename, _, _ := runtime.Caller(0)
		p := path.Join(path.Dir(filename), name)
		convertFile(p)
	}

}

func convertFile(name string) {
	ct, err := ioutil.ReadFile(name)
	if err != nil {
		panic(err)
	}

	testListOut := []TestsOut{}
	testsList := []Tests{}
	err = json.Unmarshal(ct, &testsList)

	if err != nil {
		panic(err)
	}

	for _, tests := range testsList {
		testsOut := TestsOut{Title: tests.Title}

		for _, test := range tests.Cases {
			fmt.Println(test.Program)

			jsonInput, _ := json.Marshal(test.Input)

			cmd := exec.Command("jq", "["+test.Program+" | tostring]")
			var outb, errb bytes.Buffer
			cmd.Stdout = &outb
			cmd.Stderr = &errb
			cmd.Stdin = bytes.NewReader(jsonInput)
			cmd.Run()

			var outJSON []string
			if len(outb.String()) > 0 {
				err = json.Unmarshal(outb.Bytes(), &outJSON)
				if err != nil {
					panic(err)
				}
			}

			caseOut := CaseOut{
				Program: test.Program,
				Input:   string(jsonInput),
				Output:  outJSON,
				Error:   errb.String(),
			}

			testsOut.Cases = append(testsOut.Cases, caseOut)
		}

		testListOut = append(testListOut, testsOut)
	}

	asJSON, _ := json.MarshalIndent(testListOut, "", "  ")

	ioutil.WriteFile(name[:len(name)-8]+".json", asJSON, 0666)

	fmt.Println(string(asJSON))
}
