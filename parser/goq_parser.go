// Generated from Goq.g4 by ANTLR 4.7.

package parser // Goq

import (
	"fmt"
	"reflect"
	"strconv"

	"github.com/antlr/antlr4/runtime/Go/antlr"
)

// Suppress unused import errors
var _ = fmt.Printf
var _ = reflect.Copy
var _ = strconv.Itoa

var parserATN = []uint16{
	3, 24715, 42794, 33075, 47597, 16764, 15335, 30598, 22884, 3, 37, 154,
	4, 2, 9, 2, 4, 3, 9, 3, 4, 4, 9, 4, 4, 5, 9, 5, 4, 6, 9, 6, 4, 7, 9, 7,
	3, 2, 3, 2, 3, 2, 3, 2, 3, 2, 3, 2, 3, 2, 6, 2, 22, 10, 2, 13, 2, 14, 2,
	23, 3, 2, 3, 2, 3, 2, 3, 2, 3, 2, 3, 2, 7, 2, 32, 10, 2, 12, 2, 14, 2,
	35, 11, 2, 5, 2, 37, 10, 2, 3, 2, 5, 2, 40, 10, 2, 3, 2, 3, 2, 3, 2, 3,
	2, 3, 2, 3, 2, 3, 2, 3, 2, 3, 2, 3, 2, 3, 2, 3, 2, 7, 2, 54, 10, 2, 12,
	2, 14, 2, 57, 11, 2, 5, 2, 59, 10, 2, 3, 2, 5, 2, 62, 10, 2, 3, 2, 3, 2,
	3, 2, 3, 2, 3, 2, 3, 2, 3, 2, 3, 2, 7, 2, 72, 10, 2, 12, 2, 14, 2, 75,
	11, 2, 5, 2, 77, 10, 2, 3, 2, 5, 2, 80, 10, 2, 3, 2, 3, 2, 3, 2, 3, 2,
	3, 2, 3, 2, 3, 2, 3, 2, 3, 2, 3, 2, 3, 2, 3, 2, 3, 2, 3, 2, 3, 2, 3, 2,
	3, 2, 3, 2, 3, 2, 3, 2, 3, 2, 3, 2, 3, 2, 3, 2, 5, 2, 106, 10, 2, 3, 2,
	6, 2, 109, 10, 2, 13, 2, 14, 2, 110, 7, 2, 113, 10, 2, 12, 2, 14, 2, 116,
	11, 2, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3,
	3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 5, 3, 134, 10, 3, 3, 4, 5, 4, 137, 10, 4,
	3, 5, 3, 5, 3, 5, 5, 5, 142, 10, 5, 3, 6, 3, 6, 3, 6, 3, 6, 3, 6, 3, 6,
	5, 6, 150, 10, 6, 3, 7, 3, 7, 3, 7, 2, 3, 2, 8, 2, 4, 6, 8, 10, 12, 2,
	7, 3, 2, 16, 18, 3, 2, 19, 20, 3, 2, 21, 24, 3, 2, 25, 26, 3, 2, 27, 28,
	2, 184, 2, 79, 3, 2, 2, 2, 4, 133, 3, 2, 2, 2, 6, 136, 3, 2, 2, 2, 8, 138,
	3, 2, 2, 2, 10, 149, 3, 2, 2, 2, 12, 151, 3, 2, 2, 2, 14, 15, 8, 2, 1,
	2, 15, 16, 7, 3, 2, 2, 16, 17, 5, 2, 2, 2, 17, 18, 7, 4, 2, 2, 18, 80,
	3, 2, 2, 2, 19, 80, 7, 5, 2, 2, 20, 22, 5, 4, 3, 2, 21, 20, 3, 2, 2, 2,
	22, 23, 3, 2, 2, 2, 23, 21, 3, 2, 2, 2, 23, 24, 3, 2, 2, 2, 24, 80, 3,
	2, 2, 2, 25, 26, 7, 6, 2, 2, 26, 39, 7, 33, 2, 2, 27, 36, 7, 3, 2, 2, 28,
	33, 5, 12, 7, 2, 29, 30, 7, 7, 2, 2, 30, 32, 5, 12, 7, 2, 31, 29, 3, 2,
	2, 2, 32, 35, 3, 2, 2, 2, 33, 31, 3, 2, 2, 2, 33, 34, 3, 2, 2, 2, 34, 37,
	3, 2, 2, 2, 35, 33, 3, 2, 2, 2, 36, 28, 3, 2, 2, 2, 36, 37, 3, 2, 2, 2,
	37, 38, 3, 2, 2, 2, 38, 40, 7, 4, 2, 2, 39, 27, 3, 2, 2, 2, 39, 40, 3,
	2, 2, 2, 40, 41, 3, 2, 2, 2, 41, 42, 7, 8, 2, 2, 42, 43, 5, 2, 2, 2, 43,
	44, 7, 7, 2, 2, 44, 80, 3, 2, 2, 2, 45, 80, 7, 32, 2, 2, 46, 80, 7, 35,
	2, 2, 47, 80, 7, 34, 2, 2, 48, 61, 7, 33, 2, 2, 49, 58, 7, 3, 2, 2, 50,
	55, 5, 2, 2, 2, 51, 52, 7, 7, 2, 2, 52, 54, 5, 2, 2, 2, 53, 51, 3, 2, 2,
	2, 54, 57, 3, 2, 2, 2, 55, 53, 3, 2, 2, 2, 55, 56, 3, 2, 2, 2, 56, 59,
	3, 2, 2, 2, 57, 55, 3, 2, 2, 2, 58, 50, 3, 2, 2, 2, 58, 59, 3, 2, 2, 2,
	59, 60, 3, 2, 2, 2, 60, 62, 7, 4, 2, 2, 61, 49, 3, 2, 2, 2, 61, 62, 3,
	2, 2, 2, 62, 80, 3, 2, 2, 2, 63, 64, 7, 11, 2, 2, 64, 65, 5, 2, 2, 2, 65,
	66, 7, 12, 2, 2, 66, 80, 3, 2, 2, 2, 67, 76, 7, 13, 2, 2, 68, 73, 5, 8,
	5, 2, 69, 70, 7, 14, 2, 2, 70, 72, 5, 8, 5, 2, 71, 69, 3, 2, 2, 2, 72,
	75, 3, 2, 2, 2, 73, 71, 3, 2, 2, 2, 73, 74, 3, 2, 2, 2, 74, 77, 3, 2, 2,
	2, 75, 73, 3, 2, 2, 2, 76, 68, 3, 2, 2, 2, 76, 77, 3, 2, 2, 2, 77, 78,
	3, 2, 2, 2, 78, 80, 7, 15, 2, 2, 79, 14, 3, 2, 2, 2, 79, 19, 3, 2, 2, 2,
	79, 21, 3, 2, 2, 2, 79, 25, 3, 2, 2, 2, 79, 45, 3, 2, 2, 2, 79, 46, 3,
	2, 2, 2, 79, 47, 3, 2, 2, 2, 79, 48, 3, 2, 2, 2, 79, 63, 3, 2, 2, 2, 79,
	67, 3, 2, 2, 2, 80, 114, 3, 2, 2, 2, 81, 82, 12, 9, 2, 2, 82, 83, 9, 2,
	2, 2, 83, 113, 5, 2, 2, 10, 84, 85, 12, 8, 2, 2, 85, 86, 9, 3, 2, 2, 86,
	113, 5, 2, 2, 9, 87, 88, 12, 7, 2, 2, 88, 89, 9, 4, 2, 2, 89, 113, 5, 2,
	2, 8, 90, 91, 12, 6, 2, 2, 91, 92, 9, 5, 2, 2, 92, 113, 5, 2, 2, 7, 93,
	94, 12, 5, 2, 2, 94, 95, 9, 6, 2, 2, 95, 113, 5, 2, 2, 6, 96, 97, 12, 4,
	2, 2, 97, 98, 7, 14, 2, 2, 98, 113, 5, 2, 2, 5, 99, 100, 12, 17, 2, 2,
	100, 113, 7, 9, 2, 2, 101, 102, 12, 16, 2, 2, 102, 113, 7, 10, 2, 2, 103,
	108, 12, 3, 2, 2, 104, 106, 7, 29, 2, 2, 105, 104, 3, 2, 2, 2, 105, 106,
	3, 2, 2, 2, 106, 107, 3, 2, 2, 2, 107, 109, 5, 2, 2, 2, 108, 105, 3, 2,
	2, 2, 109, 110, 3, 2, 2, 2, 110, 108, 3, 2, 2, 2, 110, 111, 3, 2, 2, 2,
	111, 113, 3, 2, 2, 2, 112, 81, 3, 2, 2, 2, 112, 84, 3, 2, 2, 2, 112, 87,
	3, 2, 2, 2, 112, 90, 3, 2, 2, 2, 112, 93, 3, 2, 2, 2, 112, 96, 3, 2, 2,
	2, 112, 99, 3, 2, 2, 2, 112, 101, 3, 2, 2, 2, 112, 103, 3, 2, 2, 2, 113,
	116, 3, 2, 2, 2, 114, 112, 3, 2, 2, 2, 114, 115, 3, 2, 2, 2, 115, 3, 3,
	2, 2, 2, 116, 114, 3, 2, 2, 2, 117, 118, 7, 30, 2, 2, 118, 134, 7, 33,
	2, 2, 119, 120, 7, 30, 2, 2, 120, 121, 7, 11, 2, 2, 121, 122, 5, 2, 2,
	2, 122, 123, 7, 12, 2, 2, 123, 134, 3, 2, 2, 2, 124, 125, 7, 30, 2, 2,
	125, 126, 7, 11, 2, 2, 126, 127, 5, 6, 4, 2, 127, 128, 7, 8, 2, 2, 128,
	129, 5, 6, 4, 2, 129, 130, 7, 12, 2, 2, 130, 134, 3, 2, 2, 2, 131, 134,
	7, 31, 2, 2, 132, 134, 7, 30, 2, 2, 133, 117, 3, 2, 2, 2, 133, 119, 3,
	2, 2, 2, 133, 124, 3, 2, 2, 2, 133, 131, 3, 2, 2, 2, 133, 132, 3, 2, 2,
	2, 134, 5, 3, 2, 2, 2, 135, 137, 5, 2, 2, 2, 136, 135, 3, 2, 2, 2, 136,
	137, 3, 2, 2, 2, 137, 7, 3, 2, 2, 2, 138, 141, 5, 10, 6, 2, 139, 140, 7,
	8, 2, 2, 140, 142, 5, 2, 2, 2, 141, 139, 3, 2, 2, 2, 141, 142, 3, 2, 2,
	2, 142, 9, 3, 2, 2, 2, 143, 150, 7, 33, 2, 2, 144, 150, 7, 34, 2, 2, 145,
	146, 7, 3, 2, 2, 146, 147, 5, 2, 2, 2, 147, 148, 7, 4, 2, 2, 148, 150,
	3, 2, 2, 2, 149, 143, 3, 2, 2, 2, 149, 144, 3, 2, 2, 2, 149, 145, 3, 2,
	2, 2, 150, 11, 3, 2, 2, 2, 151, 152, 7, 33, 2, 2, 152, 13, 3, 2, 2, 2,
	20, 23, 33, 36, 39, 55, 58, 61, 73, 76, 79, 105, 110, 112, 114, 133, 136,
	141, 149,
}
var deserializer = antlr.NewATNDeserializer(nil)
var deserializedATN = deserializer.DeserializeFromUInt16(parserATN)

var literalNames = []string{
	"", "'('", "')'", "'null'", "'def'", "';'", "':'", "'[]'", "'?'", "'['",
	"']'", "'{'", "','", "'}'", "'*'", "'/'", "'%'", "'+'", "'-'", "'<'", "'<='",
	"'>'", "'>='", "'and'", "'or'", "'=='", "'!='", "'|'", "'.'", "'..'",
}
var symbolicNames = []string{
	"", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",
	"", "", "", "", "", "", "", "", "", "", "", "", "BOOL", "NAME", "STRING",
	"INT", "NUMBER", "WS",
}

var ruleNames = []string{
	"expression", "basicExpression", "optionalExpression", "objectEntry", "key",
	"param",
}
var decisionToDFA = make([]*antlr.DFA, len(deserializedATN.DecisionToState))

func init() {
	for index, ds := range deserializedATN.DecisionToState {
		decisionToDFA[index] = antlr.NewDFA(ds, index)
	}
}

type GoqParser struct {
	*antlr.BaseParser
}

func NewGoqParser(input antlr.TokenStream) *GoqParser {
	this := new(GoqParser)

	this.BaseParser = antlr.NewBaseParser(input)

	this.Interpreter = antlr.NewParserATNSimulator(this, deserializedATN, decisionToDFA, antlr.NewPredictionContextCache())
	this.RuleNames = ruleNames
	this.LiteralNames = literalNames
	this.SymbolicNames = symbolicNames
	this.GrammarFileName = "Goq.g4"

	return this
}

// GoqParser tokens.
const (
	GoqParserEOF    = antlr.TokenEOF
	GoqParserT__0   = 1
	GoqParserT__1   = 2
	GoqParserT__2   = 3
	GoqParserT__3   = 4
	GoqParserT__4   = 5
	GoqParserT__5   = 6
	GoqParserT__6   = 7
	GoqParserT__7   = 8
	GoqParserT__8   = 9
	GoqParserT__9   = 10
	GoqParserT__10  = 11
	GoqParserT__11  = 12
	GoqParserT__12  = 13
	GoqParserT__13  = 14
	GoqParserT__14  = 15
	GoqParserT__15  = 16
	GoqParserT__16  = 17
	GoqParserT__17  = 18
	GoqParserT__18  = 19
	GoqParserT__19  = 20
	GoqParserT__20  = 21
	GoqParserT__21  = 22
	GoqParserT__22  = 23
	GoqParserT__23  = 24
	GoqParserT__24  = 25
	GoqParserT__25  = 26
	GoqParserT__26  = 27
	GoqParserT__27  = 28
	GoqParserT__28  = 29
	GoqParserBOOL   = 30
	GoqParserNAME   = 31
	GoqParserSTRING = 32
	GoqParserINT    = 33
	GoqParserNUMBER = 34
	GoqParserWS     = 35
)

// GoqParser rules.
const (
	GoqParserRULE_expression         = 0
	GoqParserRULE_basicExpression    = 1
	GoqParserRULE_optionalExpression = 2
	GoqParserRULE_objectEntry        = 3
	GoqParserRULE_key                = 4
	GoqParserRULE_param              = 5
)

// IExpressionContext is an interface to support dynamic dispatch.
type IExpressionContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsExpressionContext differentiates from other interfaces.
	IsExpressionContext()
}

type ExpressionContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyExpressionContext() *ExpressionContext {
	var p = new(ExpressionContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = GoqParserRULE_expression
	return p
}

func (*ExpressionContext) IsExpressionContext() {}

func NewExpressionContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *ExpressionContext {
	var p = new(ExpressionContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = GoqParserRULE_expression

	return p
}

func (s *ExpressionContext) GetParser() antlr.Parser { return s.parser }

func (s *ExpressionContext) CopyFrom(ctx *ExpressionContext) {
	s.BaseParserRuleContext.CopyFrom(ctx.BaseParserRuleContext)
}

func (s *ExpressionContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *ExpressionContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

type LgOperatorsContext struct {
	*ExpressionContext
	op antlr.Token
}

func NewLgOperatorsContext(parser antlr.Parser, ctx antlr.ParserRuleContext) *LgOperatorsContext {
	var p = new(LgOperatorsContext)

	p.ExpressionContext = NewEmptyExpressionContext()
	p.parser = parser
	p.CopyFrom(ctx.(*ExpressionContext))

	return p
}

func (s *LgOperatorsContext) GetOp() antlr.Token { return s.op }

func (s *LgOperatorsContext) SetOp(v antlr.Token) { s.op = v }

func (s *LgOperatorsContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *LgOperatorsContext) AllExpression() []IExpressionContext {
	var ts = s.GetTypedRuleContexts(reflect.TypeOf((*IExpressionContext)(nil)).Elem())
	var tst = make([]IExpressionContext, len(ts))

	for i, t := range ts {
		if t != nil {
			tst[i] = t.(IExpressionContext)
		}
	}

	return tst
}

func (s *LgOperatorsContext) Expression(i int) IExpressionContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IExpressionContext)(nil)).Elem(), i)

	if t == nil {
		return nil
	}

	return t.(IExpressionContext)
}

func (s *LgOperatorsContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case GoqVisitor:
		return t.VisitLgOperators(s)

	default:
		return t.VisitChildren(s)
	}
}

type BasicExpressionsContext struct {
	*ExpressionContext
}

func NewBasicExpressionsContext(parser antlr.Parser, ctx antlr.ParserRuleContext) *BasicExpressionsContext {
	var p = new(BasicExpressionsContext)

	p.ExpressionContext = NewEmptyExpressionContext()
	p.parser = parser
	p.CopyFrom(ctx.(*ExpressionContext))

	return p
}

func (s *BasicExpressionsContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *BasicExpressionsContext) AllBasicExpression() []IBasicExpressionContext {
	var ts = s.GetTypedRuleContexts(reflect.TypeOf((*IBasicExpressionContext)(nil)).Elem())
	var tst = make([]IBasicExpressionContext, len(ts))

	for i, t := range ts {
		if t != nil {
			tst[i] = t.(IBasicExpressionContext)
		}
	}

	return tst
}

func (s *BasicExpressionsContext) BasicExpression(i int) IBasicExpressionContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IBasicExpressionContext)(nil)).Elem(), i)

	if t == nil {
		return nil
	}

	return t.(IBasicExpressionContext)
}

func (s *BasicExpressionsContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case GoqVisitor:
		return t.VisitBasicExpressions(s)

	default:
		return t.VisitChildren(s)
	}
}

type DefContext struct {
	*ExpressionContext
}

func NewDefContext(parser antlr.Parser, ctx antlr.ParserRuleContext) *DefContext {
	var p = new(DefContext)

	p.ExpressionContext = NewEmptyExpressionContext()
	p.parser = parser
	p.CopyFrom(ctx.(*ExpressionContext))

	return p
}

func (s *DefContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *DefContext) NAME() antlr.TerminalNode {
	return s.GetToken(GoqParserNAME, 0)
}

func (s *DefContext) Expression() IExpressionContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IExpressionContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IExpressionContext)
}

func (s *DefContext) AllParam() []IParamContext {
	var ts = s.GetTypedRuleContexts(reflect.TypeOf((*IParamContext)(nil)).Elem())
	var tst = make([]IParamContext, len(ts))

	for i, t := range ts {
		if t != nil {
			tst[i] = t.(IParamContext)
		}
	}

	return tst
}

func (s *DefContext) Param(i int) IParamContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IParamContext)(nil)).Elem(), i)

	if t == nil {
		return nil
	}

	return t.(IParamContext)
}

func (s *DefContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case GoqVisitor:
		return t.VisitDef(s)

	default:
		return t.VisitChildren(s)
	}
}

type BoolContext struct {
	*ExpressionContext
}

func NewBoolContext(parser antlr.Parser, ctx antlr.ParserRuleContext) *BoolContext {
	var p = new(BoolContext)

	p.ExpressionContext = NewEmptyExpressionContext()
	p.parser = parser
	p.CopyFrom(ctx.(*ExpressionContext))

	return p
}

func (s *BoolContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *BoolContext) BOOL() antlr.TerminalNode {
	return s.GetToken(GoqParserBOOL, 0)
}

func (s *BoolContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case GoqVisitor:
		return t.VisitBool(s)

	default:
		return t.VisitChildren(s)
	}
}

type StringContext struct {
	*ExpressionContext
}

func NewStringContext(parser antlr.Parser, ctx antlr.ParserRuleContext) *StringContext {
	var p = new(StringContext)

	p.ExpressionContext = NewEmptyExpressionContext()
	p.parser = parser
	p.CopyFrom(ctx.(*ExpressionContext))

	return p
}

func (s *StringContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *StringContext) STRING() antlr.TerminalNode {
	return s.GetToken(GoqParserSTRING, 0)
}

func (s *StringContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case GoqVisitor:
		return t.VisitString(s)

	default:
		return t.VisitChildren(s)
	}
}

type BoolOperatorsContext struct {
	*ExpressionContext
	op antlr.Token
}

func NewBoolOperatorsContext(parser antlr.Parser, ctx antlr.ParserRuleContext) *BoolOperatorsContext {
	var p = new(BoolOperatorsContext)

	p.ExpressionContext = NewEmptyExpressionContext()
	p.parser = parser
	p.CopyFrom(ctx.(*ExpressionContext))

	return p
}

func (s *BoolOperatorsContext) GetOp() antlr.Token { return s.op }

func (s *BoolOperatorsContext) SetOp(v antlr.Token) { s.op = v }

func (s *BoolOperatorsContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *BoolOperatorsContext) AllExpression() []IExpressionContext {
	var ts = s.GetTypedRuleContexts(reflect.TypeOf((*IExpressionContext)(nil)).Elem())
	var tst = make([]IExpressionContext, len(ts))

	for i, t := range ts {
		if t != nil {
			tst[i] = t.(IExpressionContext)
		}
	}

	return tst
}

func (s *BoolOperatorsContext) Expression(i int) IExpressionContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IExpressionContext)(nil)).Elem(), i)

	if t == nil {
		return nil
	}

	return t.(IExpressionContext)
}

func (s *BoolOperatorsContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case GoqVisitor:
		return t.VisitBoolOperators(s)

	default:
		return t.VisitChildren(s)
	}
}

type IntegerContext struct {
	*ExpressionContext
}

func NewIntegerContext(parser antlr.Parser, ctx antlr.ParserRuleContext) *IntegerContext {
	var p = new(IntegerContext)

	p.ExpressionContext = NewEmptyExpressionContext()
	p.parser = parser
	p.CopyFrom(ctx.(*ExpressionContext))

	return p
}

func (s *IntegerContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *IntegerContext) INT() antlr.TerminalNode {
	return s.GetToken(GoqParserINT, 0)
}

func (s *IntegerContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case GoqVisitor:
		return t.VisitInteger(s)

	default:
		return t.VisitChildren(s)
	}
}

type AnyContext struct {
	*ExpressionContext
}

func NewAnyContext(parser antlr.Parser, ctx antlr.ParserRuleContext) *AnyContext {
	var p = new(AnyContext)

	p.ExpressionContext = NewEmptyExpressionContext()
	p.parser = parser
	p.CopyFrom(ctx.(*ExpressionContext))

	return p
}

func (s *AnyContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *AnyContext) Expression() IExpressionContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IExpressionContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IExpressionContext)
}

func (s *AnyContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case GoqVisitor:
		return t.VisitAny(s)

	default:
		return t.VisitChildren(s)
	}
}

type ExpressionsContext struct {
	*ExpressionContext
}

func NewExpressionsContext(parser antlr.Parser, ctx antlr.ParserRuleContext) *ExpressionsContext {
	var p = new(ExpressionsContext)

	p.ExpressionContext = NewEmptyExpressionContext()
	p.parser = parser
	p.CopyFrom(ctx.(*ExpressionContext))

	return p
}

func (s *ExpressionsContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *ExpressionsContext) AllExpression() []IExpressionContext {
	var ts = s.GetTypedRuleContexts(reflect.TypeOf((*IExpressionContext)(nil)).Elem())
	var tst = make([]IExpressionContext, len(ts))

	for i, t := range ts {
		if t != nil {
			tst[i] = t.(IExpressionContext)
		}
	}

	return tst
}

func (s *ExpressionsContext) Expression(i int) IExpressionContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IExpressionContext)(nil)).Elem(), i)

	if t == nil {
		return nil
	}

	return t.(IExpressionContext)
}

func (s *ExpressionsContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case GoqVisitor:
		return t.VisitExpressions(s)

	default:
		return t.VisitChildren(s)
	}
}

type EqOperatorsContext struct {
	*ExpressionContext
	op antlr.Token
}

func NewEqOperatorsContext(parser antlr.Parser, ctx antlr.ParserRuleContext) *EqOperatorsContext {
	var p = new(EqOperatorsContext)

	p.ExpressionContext = NewEmptyExpressionContext()
	p.parser = parser
	p.CopyFrom(ctx.(*ExpressionContext))

	return p
}

func (s *EqOperatorsContext) GetOp() antlr.Token { return s.op }

func (s *EqOperatorsContext) SetOp(v antlr.Token) { s.op = v }

func (s *EqOperatorsContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *EqOperatorsContext) AllExpression() []IExpressionContext {
	var ts = s.GetTypedRuleContexts(reflect.TypeOf((*IExpressionContext)(nil)).Elem())
	var tst = make([]IExpressionContext, len(ts))

	for i, t := range ts {
		if t != nil {
			tst[i] = t.(IExpressionContext)
		}
	}

	return tst
}

func (s *EqOperatorsContext) Expression(i int) IExpressionContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IExpressionContext)(nil)).Elem(), i)

	if t == nil {
		return nil
	}

	return t.(IExpressionContext)
}

func (s *EqOperatorsContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case GoqVisitor:
		return t.VisitEqOperators(s)

	default:
		return t.VisitChildren(s)
	}
}

type CommaContext struct {
	*ExpressionContext
}

func NewCommaContext(parser antlr.Parser, ctx antlr.ParserRuleContext) *CommaContext {
	var p = new(CommaContext)

	p.ExpressionContext = NewEmptyExpressionContext()
	p.parser = parser
	p.CopyFrom(ctx.(*ExpressionContext))

	return p
}

func (s *CommaContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *CommaContext) AllExpression() []IExpressionContext {
	var ts = s.GetTypedRuleContexts(reflect.TypeOf((*IExpressionContext)(nil)).Elem())
	var tst = make([]IExpressionContext, len(ts))

	for i, t := range ts {
		if t != nil {
			tst[i] = t.(IExpressionContext)
		}
	}

	return tst
}

func (s *CommaContext) Expression(i int) IExpressionContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IExpressionContext)(nil)).Elem(), i)

	if t == nil {
		return nil
	}

	return t.(IExpressionContext)
}

func (s *CommaContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case GoqVisitor:
		return t.VisitComma(s)

	default:
		return t.VisitChildren(s)
	}
}

type NullContext struct {
	*ExpressionContext
}

func NewNullContext(parser antlr.Parser, ctx antlr.ParserRuleContext) *NullContext {
	var p = new(NullContext)

	p.ExpressionContext = NewEmptyExpressionContext()
	p.parser = parser
	p.CopyFrom(ctx.(*ExpressionContext))

	return p
}

func (s *NullContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *NullContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case GoqVisitor:
		return t.VisitNull(s)

	default:
		return t.VisitChildren(s)
	}
}

type ArrayContext struct {
	*ExpressionContext
}

func NewArrayContext(parser antlr.Parser, ctx antlr.ParserRuleContext) *ArrayContext {
	var p = new(ArrayContext)

	p.ExpressionContext = NewEmptyExpressionContext()
	p.parser = parser
	p.CopyFrom(ctx.(*ExpressionContext))

	return p
}

func (s *ArrayContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *ArrayContext) Expression() IExpressionContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IExpressionContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IExpressionContext)
}

func (s *ArrayContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case GoqVisitor:
		return t.VisitArray(s)

	default:
		return t.VisitChildren(s)
	}
}

type FunctionContext struct {
	*ExpressionContext
}

func NewFunctionContext(parser antlr.Parser, ctx antlr.ParserRuleContext) *FunctionContext {
	var p = new(FunctionContext)

	p.ExpressionContext = NewEmptyExpressionContext()
	p.parser = parser
	p.CopyFrom(ctx.(*ExpressionContext))

	return p
}

func (s *FunctionContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *FunctionContext) NAME() antlr.TerminalNode {
	return s.GetToken(GoqParserNAME, 0)
}

func (s *FunctionContext) AllExpression() []IExpressionContext {
	var ts = s.GetTypedRuleContexts(reflect.TypeOf((*IExpressionContext)(nil)).Elem())
	var tst = make([]IExpressionContext, len(ts))

	for i, t := range ts {
		if t != nil {
			tst[i] = t.(IExpressionContext)
		}
	}

	return tst
}

func (s *FunctionContext) Expression(i int) IExpressionContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IExpressionContext)(nil)).Elem(), i)

	if t == nil {
		return nil
	}

	return t.(IExpressionContext)
}

func (s *FunctionContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case GoqVisitor:
		return t.VisitFunction(s)

	default:
		return t.VisitChildren(s)
	}
}

type MulOperationsContext struct {
	*ExpressionContext
	op antlr.Token
}

func NewMulOperationsContext(parser antlr.Parser, ctx antlr.ParserRuleContext) *MulOperationsContext {
	var p = new(MulOperationsContext)

	p.ExpressionContext = NewEmptyExpressionContext()
	p.parser = parser
	p.CopyFrom(ctx.(*ExpressionContext))

	return p
}

func (s *MulOperationsContext) GetOp() antlr.Token { return s.op }

func (s *MulOperationsContext) SetOp(v antlr.Token) { s.op = v }

func (s *MulOperationsContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *MulOperationsContext) AllExpression() []IExpressionContext {
	var ts = s.GetTypedRuleContexts(reflect.TypeOf((*IExpressionContext)(nil)).Elem())
	var tst = make([]IExpressionContext, len(ts))

	for i, t := range ts {
		if t != nil {
			tst[i] = t.(IExpressionContext)
		}
	}

	return tst
}

func (s *MulOperationsContext) Expression(i int) IExpressionContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IExpressionContext)(nil)).Elem(), i)

	if t == nil {
		return nil
	}

	return t.(IExpressionContext)
}

func (s *MulOperationsContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case GoqVisitor:
		return t.VisitMulOperations(s)

	default:
		return t.VisitChildren(s)
	}
}

type AddOperationsContext struct {
	*ExpressionContext
	op antlr.Token
}

func NewAddOperationsContext(parser antlr.Parser, ctx antlr.ParserRuleContext) *AddOperationsContext {
	var p = new(AddOperationsContext)

	p.ExpressionContext = NewEmptyExpressionContext()
	p.parser = parser
	p.CopyFrom(ctx.(*ExpressionContext))

	return p
}

func (s *AddOperationsContext) GetOp() antlr.Token { return s.op }

func (s *AddOperationsContext) SetOp(v antlr.Token) { s.op = v }

func (s *AddOperationsContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *AddOperationsContext) AllExpression() []IExpressionContext {
	var ts = s.GetTypedRuleContexts(reflect.TypeOf((*IExpressionContext)(nil)).Elem())
	var tst = make([]IExpressionContext, len(ts))

	for i, t := range ts {
		if t != nil {
			tst[i] = t.(IExpressionContext)
		}
	}

	return tst
}

func (s *AddOperationsContext) Expression(i int) IExpressionContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IExpressionContext)(nil)).Elem(), i)

	if t == nil {
		return nil
	}

	return t.(IExpressionContext)
}

func (s *AddOperationsContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case GoqVisitor:
		return t.VisitAddOperations(s)

	default:
		return t.VisitChildren(s)
	}
}

type NoErrorContext struct {
	*ExpressionContext
}

func NewNoErrorContext(parser antlr.Parser, ctx antlr.ParserRuleContext) *NoErrorContext {
	var p = new(NoErrorContext)

	p.ExpressionContext = NewEmptyExpressionContext()
	p.parser = parser
	p.CopyFrom(ctx.(*ExpressionContext))

	return p
}

func (s *NoErrorContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *NoErrorContext) Expression() IExpressionContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IExpressionContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IExpressionContext)
}

func (s *NoErrorContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case GoqVisitor:
		return t.VisitNoError(s)

	default:
		return t.VisitChildren(s)
	}
}

type GroupContext struct {
	*ExpressionContext
}

func NewGroupContext(parser antlr.Parser, ctx antlr.ParserRuleContext) *GroupContext {
	var p = new(GroupContext)

	p.ExpressionContext = NewEmptyExpressionContext()
	p.parser = parser
	p.CopyFrom(ctx.(*ExpressionContext))

	return p
}

func (s *GroupContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *GroupContext) Expression() IExpressionContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IExpressionContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IExpressionContext)
}

func (s *GroupContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case GoqVisitor:
		return t.VisitGroup(s)

	default:
		return t.VisitChildren(s)
	}
}

type ObjectContext struct {
	*ExpressionContext
}

func NewObjectContext(parser antlr.Parser, ctx antlr.ParserRuleContext) *ObjectContext {
	var p = new(ObjectContext)

	p.ExpressionContext = NewEmptyExpressionContext()
	p.parser = parser
	p.CopyFrom(ctx.(*ExpressionContext))

	return p
}

func (s *ObjectContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *ObjectContext) AllObjectEntry() []IObjectEntryContext {
	var ts = s.GetTypedRuleContexts(reflect.TypeOf((*IObjectEntryContext)(nil)).Elem())
	var tst = make([]IObjectEntryContext, len(ts))

	for i, t := range ts {
		if t != nil {
			tst[i] = t.(IObjectEntryContext)
		}
	}

	return tst
}

func (s *ObjectContext) ObjectEntry(i int) IObjectEntryContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IObjectEntryContext)(nil)).Elem(), i)

	if t == nil {
		return nil
	}

	return t.(IObjectEntryContext)
}

func (s *ObjectContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case GoqVisitor:
		return t.VisitObject(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *GoqParser) Expression() (localctx IExpressionContext) {
	return p.expression(0)
}

func (p *GoqParser) expression(_p int) (localctx IExpressionContext) {
	var _parentctx antlr.ParserRuleContext = p.GetParserRuleContext()
	_parentState := p.GetState()
	localctx = NewExpressionContext(p, p.GetParserRuleContext(), _parentState)
	var _prevctx IExpressionContext = localctx
	var _ antlr.ParserRuleContext = _prevctx // TODO: To prevent unused variable warning.
	_startState := 0
	p.EnterRecursionRule(localctx, 0, GoqParserRULE_expression, _p)
	var _la int

	defer func() {
		p.UnrollRecursionContexts(_parentctx)
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	var _alt int

	p.EnterOuterAlt(localctx, 1)
	p.SetState(77)
	p.GetErrorHandler().Sync(p)

	switch p.GetTokenStream().LA(1) {
	case GoqParserT__0:
		localctx = NewGroupContext(p, localctx)
		p.SetParserRuleContext(localctx)
		_prevctx = localctx

		{
			p.SetState(13)
			p.Match(GoqParserT__0)
		}
		{
			p.SetState(14)
			p.expression(0)
		}
		{
			p.SetState(15)
			p.Match(GoqParserT__1)
		}

	case GoqParserT__2:
		localctx = NewNullContext(p, localctx)
		p.SetParserRuleContext(localctx)
		_prevctx = localctx
		{
			p.SetState(17)
			p.Match(GoqParserT__2)
		}

	case GoqParserT__27, GoqParserT__28:
		localctx = NewBasicExpressionsContext(p, localctx)
		p.SetParserRuleContext(localctx)
		_prevctx = localctx
		p.SetState(19)
		p.GetErrorHandler().Sync(p)
		_alt = 1
		for ok := true; ok; ok = _alt != 2 && _alt != antlr.ATNInvalidAltNumber {
			switch _alt {
			case 1:
				{
					p.SetState(18)
					p.BasicExpression()
				}

			default:
				panic(antlr.NewNoViableAltException(p, nil, nil, nil, nil, nil))
			}

			p.SetState(21)
			p.GetErrorHandler().Sync(p)
			_alt = p.GetInterpreter().AdaptivePredict(p.GetTokenStream(), 0, p.GetParserRuleContext())
		}

	case GoqParserT__3:
		localctx = NewDefContext(p, localctx)
		p.SetParserRuleContext(localctx)
		_prevctx = localctx
		{
			p.SetState(23)
			p.Match(GoqParserT__3)
		}
		{
			p.SetState(24)
			p.Match(GoqParserNAME)
		}
		p.SetState(37)
		p.GetErrorHandler().Sync(p)
		_la = p.GetTokenStream().LA(1)

		if _la == GoqParserT__0 {
			{
				p.SetState(25)
				p.Match(GoqParserT__0)
			}
			p.SetState(34)
			p.GetErrorHandler().Sync(p)
			_la = p.GetTokenStream().LA(1)

			if _la == GoqParserNAME {
				{
					p.SetState(26)
					p.Param()
				}
				p.SetState(31)
				p.GetErrorHandler().Sync(p)
				_la = p.GetTokenStream().LA(1)

				for _la == GoqParserT__4 {
					{
						p.SetState(27)
						p.Match(GoqParserT__4)
					}
					{
						p.SetState(28)
						p.Param()
					}

					p.SetState(33)
					p.GetErrorHandler().Sync(p)
					_la = p.GetTokenStream().LA(1)
				}

			}
			{
				p.SetState(36)
				p.Match(GoqParserT__1)
			}

		}
		{
			p.SetState(39)
			p.Match(GoqParserT__5)
		}
		{
			p.SetState(40)
			p.expression(0)
		}
		{
			p.SetState(41)
			p.Match(GoqParserT__4)
		}

	case GoqParserBOOL:
		localctx = NewBoolContext(p, localctx)
		p.SetParserRuleContext(localctx)
		_prevctx = localctx
		{
			p.SetState(43)
			p.Match(GoqParserBOOL)
		}

	case GoqParserINT:
		localctx = NewIntegerContext(p, localctx)
		p.SetParserRuleContext(localctx)
		_prevctx = localctx
		{
			p.SetState(44)
			p.Match(GoqParserINT)
		}

	case GoqParserSTRING:
		localctx = NewStringContext(p, localctx)
		p.SetParserRuleContext(localctx)
		_prevctx = localctx
		{
			p.SetState(45)
			p.Match(GoqParserSTRING)
		}

	case GoqParserNAME:
		localctx = NewFunctionContext(p, localctx)
		p.SetParserRuleContext(localctx)
		_prevctx = localctx
		{
			p.SetState(46)
			p.Match(GoqParserNAME)
		}
		p.SetState(59)
		p.GetErrorHandler().Sync(p)

		if p.GetInterpreter().AdaptivePredict(p.GetTokenStream(), 6, p.GetParserRuleContext()) == 1 {
			{
				p.SetState(47)
				p.Match(GoqParserT__0)
			}
			p.SetState(56)
			p.GetErrorHandler().Sync(p)
			_la = p.GetTokenStream().LA(1)

			if (((_la)&-(0x1f+1)) == 0 && ((1<<uint(_la))&((1<<GoqParserT__0)|(1<<GoqParserT__2)|(1<<GoqParserT__3)|(1<<GoqParserT__8)|(1<<GoqParserT__10)|(1<<GoqParserT__27)|(1<<GoqParserT__28)|(1<<GoqParserBOOL)|(1<<GoqParserNAME))) != 0) || _la == GoqParserSTRING || _la == GoqParserINT {
				{
					p.SetState(48)
					p.expression(0)
				}
				p.SetState(53)
				p.GetErrorHandler().Sync(p)
				_la = p.GetTokenStream().LA(1)

				for _la == GoqParserT__4 {
					{
						p.SetState(49)
						p.Match(GoqParserT__4)
					}
					{
						p.SetState(50)
						p.expression(0)
					}

					p.SetState(55)
					p.GetErrorHandler().Sync(p)
					_la = p.GetTokenStream().LA(1)
				}

			}
			{
				p.SetState(58)
				p.Match(GoqParserT__1)
			}

		}

	case GoqParserT__8:
		localctx = NewArrayContext(p, localctx)
		p.SetParserRuleContext(localctx)
		_prevctx = localctx
		{
			p.SetState(61)
			p.Match(GoqParserT__8)
		}
		{
			p.SetState(62)
			p.expression(0)
		}
		{
			p.SetState(63)
			p.Match(GoqParserT__9)
		}

	case GoqParserT__10:
		localctx = NewObjectContext(p, localctx)
		p.SetParserRuleContext(localctx)
		_prevctx = localctx
		{
			p.SetState(65)
			p.Match(GoqParserT__10)
		}
		p.SetState(74)
		p.GetErrorHandler().Sync(p)
		_la = p.GetTokenStream().LA(1)

		if ((_la-1)&-(0x1f+1)) == 0 && ((1<<uint((_la-1)))&((1<<(GoqParserT__0-1))|(1<<(GoqParserNAME-1))|(1<<(GoqParserSTRING-1)))) != 0 {
			{
				p.SetState(66)
				p.ObjectEntry()
			}
			p.SetState(71)
			p.GetErrorHandler().Sync(p)
			_la = p.GetTokenStream().LA(1)

			for _la == GoqParserT__11 {
				{
					p.SetState(67)
					p.Match(GoqParserT__11)
				}
				{
					p.SetState(68)
					p.ObjectEntry()
				}

				p.SetState(73)
				p.GetErrorHandler().Sync(p)
				_la = p.GetTokenStream().LA(1)
			}

		}
		{
			p.SetState(76)
			p.Match(GoqParserT__12)
		}

	default:
		panic(antlr.NewNoViableAltException(p, nil, nil, nil, nil, nil))
	}
	p.GetParserRuleContext().SetStop(p.GetTokenStream().LT(-1))
	p.SetState(112)
	p.GetErrorHandler().Sync(p)
	_alt = p.GetInterpreter().AdaptivePredict(p.GetTokenStream(), 13, p.GetParserRuleContext())

	for _alt != 2 && _alt != antlr.ATNInvalidAltNumber {
		if _alt == 1 {
			if p.GetParseListeners() != nil {
				p.TriggerExitRuleEvent()
			}
			_prevctx = localctx
			p.SetState(110)
			p.GetErrorHandler().Sync(p)
			switch p.GetInterpreter().AdaptivePredict(p.GetTokenStream(), 12, p.GetParserRuleContext()) {
			case 1:
				localctx = NewMulOperationsContext(p, NewExpressionContext(p, _parentctx, _parentState))
				p.PushNewRecursionContext(localctx, _startState, GoqParserRULE_expression)
				p.SetState(79)

				if !(p.Precpred(p.GetParserRuleContext(), 7)) {
					panic(antlr.NewFailedPredicateException(p, "p.Precpred(p.GetParserRuleContext(), 7)", ""))
				}
				p.SetState(80)

				var _lt = p.GetTokenStream().LT(1)

				localctx.(*MulOperationsContext).op = _lt

				_la = p.GetTokenStream().LA(1)

				if !(((_la)&-(0x1f+1)) == 0 && ((1<<uint(_la))&((1<<GoqParserT__13)|(1<<GoqParserT__14)|(1<<GoqParserT__15))) != 0) {
					var _ri = p.GetErrorHandler().RecoverInline(p)

					localctx.(*MulOperationsContext).op = _ri
				} else {
					p.GetErrorHandler().ReportMatch(p)
					p.Consume()
				}
				{
					p.SetState(81)
					p.expression(8)
				}

			case 2:
				localctx = NewAddOperationsContext(p, NewExpressionContext(p, _parentctx, _parentState))
				p.PushNewRecursionContext(localctx, _startState, GoqParserRULE_expression)
				p.SetState(82)

				if !(p.Precpred(p.GetParserRuleContext(), 6)) {
					panic(antlr.NewFailedPredicateException(p, "p.Precpred(p.GetParserRuleContext(), 6)", ""))
				}
				p.SetState(83)

				var _lt = p.GetTokenStream().LT(1)

				localctx.(*AddOperationsContext).op = _lt

				_la = p.GetTokenStream().LA(1)

				if !(_la == GoqParserT__16 || _la == GoqParserT__17) {
					var _ri = p.GetErrorHandler().RecoverInline(p)

					localctx.(*AddOperationsContext).op = _ri
				} else {
					p.GetErrorHandler().ReportMatch(p)
					p.Consume()
				}
				{
					p.SetState(84)
					p.expression(7)
				}

			case 3:
				localctx = NewLgOperatorsContext(p, NewExpressionContext(p, _parentctx, _parentState))
				p.PushNewRecursionContext(localctx, _startState, GoqParserRULE_expression)
				p.SetState(85)

				if !(p.Precpred(p.GetParserRuleContext(), 5)) {
					panic(antlr.NewFailedPredicateException(p, "p.Precpred(p.GetParserRuleContext(), 5)", ""))
				}
				p.SetState(86)

				var _lt = p.GetTokenStream().LT(1)

				localctx.(*LgOperatorsContext).op = _lt

				_la = p.GetTokenStream().LA(1)

				if !(((_la)&-(0x1f+1)) == 0 && ((1<<uint(_la))&((1<<GoqParserT__18)|(1<<GoqParserT__19)|(1<<GoqParserT__20)|(1<<GoqParserT__21))) != 0) {
					var _ri = p.GetErrorHandler().RecoverInline(p)

					localctx.(*LgOperatorsContext).op = _ri
				} else {
					p.GetErrorHandler().ReportMatch(p)
					p.Consume()
				}
				{
					p.SetState(87)
					p.expression(6)
				}

			case 4:
				localctx = NewBoolOperatorsContext(p, NewExpressionContext(p, _parentctx, _parentState))
				p.PushNewRecursionContext(localctx, _startState, GoqParserRULE_expression)
				p.SetState(88)

				if !(p.Precpred(p.GetParserRuleContext(), 4)) {
					panic(antlr.NewFailedPredicateException(p, "p.Precpred(p.GetParserRuleContext(), 4)", ""))
				}
				p.SetState(89)

				var _lt = p.GetTokenStream().LT(1)

				localctx.(*BoolOperatorsContext).op = _lt

				_la = p.GetTokenStream().LA(1)

				if !(_la == GoqParserT__22 || _la == GoqParserT__23) {
					var _ri = p.GetErrorHandler().RecoverInline(p)

					localctx.(*BoolOperatorsContext).op = _ri
				} else {
					p.GetErrorHandler().ReportMatch(p)
					p.Consume()
				}
				{
					p.SetState(90)
					p.expression(5)
				}

			case 5:
				localctx = NewEqOperatorsContext(p, NewExpressionContext(p, _parentctx, _parentState))
				p.PushNewRecursionContext(localctx, _startState, GoqParserRULE_expression)
				p.SetState(91)

				if !(p.Precpred(p.GetParserRuleContext(), 3)) {
					panic(antlr.NewFailedPredicateException(p, "p.Precpred(p.GetParserRuleContext(), 3)", ""))
				}
				p.SetState(92)

				var _lt = p.GetTokenStream().LT(1)

				localctx.(*EqOperatorsContext).op = _lt

				_la = p.GetTokenStream().LA(1)

				if !(_la == GoqParserT__24 || _la == GoqParserT__25) {
					var _ri = p.GetErrorHandler().RecoverInline(p)

					localctx.(*EqOperatorsContext).op = _ri
				} else {
					p.GetErrorHandler().ReportMatch(p)
					p.Consume()
				}
				{
					p.SetState(93)
					p.expression(4)
				}

			case 6:
				localctx = NewCommaContext(p, NewExpressionContext(p, _parentctx, _parentState))
				p.PushNewRecursionContext(localctx, _startState, GoqParserRULE_expression)
				p.SetState(94)

				if !(p.Precpred(p.GetParserRuleContext(), 2)) {
					panic(antlr.NewFailedPredicateException(p, "p.Precpred(p.GetParserRuleContext(), 2)", ""))
				}
				{
					p.SetState(95)
					p.Match(GoqParserT__11)
				}
				{
					p.SetState(96)
					p.expression(3)
				}

			case 7:
				localctx = NewAnyContext(p, NewExpressionContext(p, _parentctx, _parentState))
				p.PushNewRecursionContext(localctx, _startState, GoqParserRULE_expression)
				p.SetState(97)

				if !(p.Precpred(p.GetParserRuleContext(), 15)) {
					panic(antlr.NewFailedPredicateException(p, "p.Precpred(p.GetParserRuleContext(), 15)", ""))
				}
				{
					p.SetState(98)
					p.Match(GoqParserT__6)
				}

			case 8:
				localctx = NewNoErrorContext(p, NewExpressionContext(p, _parentctx, _parentState))
				p.PushNewRecursionContext(localctx, _startState, GoqParserRULE_expression)
				p.SetState(99)

				if !(p.Precpred(p.GetParserRuleContext(), 14)) {
					panic(antlr.NewFailedPredicateException(p, "p.Precpred(p.GetParserRuleContext(), 14)", ""))
				}
				{
					p.SetState(100)
					p.Match(GoqParserT__7)
				}

			case 9:
				localctx = NewExpressionsContext(p, NewExpressionContext(p, _parentctx, _parentState))
				p.PushNewRecursionContext(localctx, _startState, GoqParserRULE_expression)
				p.SetState(101)

				if !(p.Precpred(p.GetParserRuleContext(), 1)) {
					panic(antlr.NewFailedPredicateException(p, "p.Precpred(p.GetParserRuleContext(), 1)", ""))
				}
				p.SetState(106)
				p.GetErrorHandler().Sync(p)
				_alt = 1
				for ok := true; ok; ok = _alt != 2 && _alt != antlr.ATNInvalidAltNumber {
					switch _alt {
					case 1:
						p.SetState(103)
						p.GetErrorHandler().Sync(p)
						_la = p.GetTokenStream().LA(1)

						if _la == GoqParserT__26 {
							{
								p.SetState(102)
								p.Match(GoqParserT__26)
							}

						}
						{
							p.SetState(105)
							p.expression(0)
						}

					default:
						panic(antlr.NewNoViableAltException(p, nil, nil, nil, nil, nil))
					}

					p.SetState(108)
					p.GetErrorHandler().Sync(p)
					_alt = p.GetInterpreter().AdaptivePredict(p.GetTokenStream(), 11, p.GetParserRuleContext())
				}

			}

		}
		p.SetState(114)
		p.GetErrorHandler().Sync(p)
		_alt = p.GetInterpreter().AdaptivePredict(p.GetTokenStream(), 13, p.GetParserRuleContext())
	}

	return localctx
}

// IBasicExpressionContext is an interface to support dynamic dispatch.
type IBasicExpressionContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsBasicExpressionContext differentiates from other interfaces.
	IsBasicExpressionContext()
}

type BasicExpressionContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyBasicExpressionContext() *BasicExpressionContext {
	var p = new(BasicExpressionContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = GoqParserRULE_basicExpression
	return p
}

func (*BasicExpressionContext) IsBasicExpressionContext() {}

func NewBasicExpressionContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *BasicExpressionContext {
	var p = new(BasicExpressionContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = GoqParserRULE_basicExpression

	return p
}

func (s *BasicExpressionContext) GetParser() antlr.Parser { return s.parser }

func (s *BasicExpressionContext) CopyFrom(ctx *BasicExpressionContext) {
	s.BaseParserRuleContext.CopyFrom(ctx.BaseParserRuleContext)
}

func (s *BasicExpressionContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *BasicExpressionContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

type RecurseDescentContext struct {
	*BasicExpressionContext
}

func NewRecurseDescentContext(parser antlr.Parser, ctx antlr.ParserRuleContext) *RecurseDescentContext {
	var p = new(RecurseDescentContext)

	p.BasicExpressionContext = NewEmptyBasicExpressionContext()
	p.parser = parser
	p.CopyFrom(ctx.(*BasicExpressionContext))

	return p
}

func (s *RecurseDescentContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *RecurseDescentContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case GoqVisitor:
		return t.VisitRecurseDescent(s)

	default:
		return t.VisitChildren(s)
	}
}

type SliceContext struct {
	*BasicExpressionContext
}

func NewSliceContext(parser antlr.Parser, ctx antlr.ParserRuleContext) *SliceContext {
	var p = new(SliceContext)

	p.BasicExpressionContext = NewEmptyBasicExpressionContext()
	p.parser = parser
	p.CopyFrom(ctx.(*BasicExpressionContext))

	return p
}

func (s *SliceContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *SliceContext) AllOptionalExpression() []IOptionalExpressionContext {
	var ts = s.GetTypedRuleContexts(reflect.TypeOf((*IOptionalExpressionContext)(nil)).Elem())
	var tst = make([]IOptionalExpressionContext, len(ts))

	for i, t := range ts {
		if t != nil {
			tst[i] = t.(IOptionalExpressionContext)
		}
	}

	return tst
}

func (s *SliceContext) OptionalExpression(i int) IOptionalExpressionContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IOptionalExpressionContext)(nil)).Elem(), i)

	if t == nil {
		return nil
	}

	return t.(IOptionalExpressionContext)
}

func (s *SliceContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case GoqVisitor:
		return t.VisitSlice(s)

	default:
		return t.VisitChildren(s)
	}
}

type IdentityContext struct {
	*BasicExpressionContext
}

func NewIdentityContext(parser antlr.Parser, ctx antlr.ParserRuleContext) *IdentityContext {
	var p = new(IdentityContext)

	p.BasicExpressionContext = NewEmptyBasicExpressionContext()
	p.parser = parser
	p.CopyFrom(ctx.(*BasicExpressionContext))

	return p
}

func (s *IdentityContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *IdentityContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case GoqVisitor:
		return t.VisitIdentity(s)

	default:
		return t.VisitChildren(s)
	}
}

type IndexContext struct {
	*BasicExpressionContext
}

func NewIndexContext(parser antlr.Parser, ctx antlr.ParserRuleContext) *IndexContext {
	var p = new(IndexContext)

	p.BasicExpressionContext = NewEmptyBasicExpressionContext()
	p.parser = parser
	p.CopyFrom(ctx.(*BasicExpressionContext))

	return p
}

func (s *IndexContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *IndexContext) Expression() IExpressionContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IExpressionContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IExpressionContext)
}

func (s *IndexContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case GoqVisitor:
		return t.VisitIndex(s)

	default:
		return t.VisitChildren(s)
	}
}

type ObjectIdentifierContext struct {
	*BasicExpressionContext
}

func NewObjectIdentifierContext(parser antlr.Parser, ctx antlr.ParserRuleContext) *ObjectIdentifierContext {
	var p = new(ObjectIdentifierContext)

	p.BasicExpressionContext = NewEmptyBasicExpressionContext()
	p.parser = parser
	p.CopyFrom(ctx.(*BasicExpressionContext))

	return p
}

func (s *ObjectIdentifierContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *ObjectIdentifierContext) NAME() antlr.TerminalNode {
	return s.GetToken(GoqParserNAME, 0)
}

func (s *ObjectIdentifierContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case GoqVisitor:
		return t.VisitObjectIdentifier(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *GoqParser) BasicExpression() (localctx IBasicExpressionContext) {
	localctx = NewBasicExpressionContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 2, GoqParserRULE_basicExpression)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.SetState(131)
	p.GetErrorHandler().Sync(p)
	switch p.GetInterpreter().AdaptivePredict(p.GetTokenStream(), 14, p.GetParserRuleContext()) {
	case 1:
		localctx = NewObjectIdentifierContext(p, localctx)
		p.EnterOuterAlt(localctx, 1)
		{
			p.SetState(115)
			p.Match(GoqParserT__27)
		}
		{
			p.SetState(116)
			p.Match(GoqParserNAME)
		}

	case 2:
		localctx = NewIndexContext(p, localctx)
		p.EnterOuterAlt(localctx, 2)
		{
			p.SetState(117)
			p.Match(GoqParserT__27)
		}
		{
			p.SetState(118)
			p.Match(GoqParserT__8)
		}
		{
			p.SetState(119)
			p.expression(0)
		}
		{
			p.SetState(120)
			p.Match(GoqParserT__9)
		}

	case 3:
		localctx = NewSliceContext(p, localctx)
		p.EnterOuterAlt(localctx, 3)
		{
			p.SetState(122)
			p.Match(GoqParserT__27)
		}
		{
			p.SetState(123)
			p.Match(GoqParserT__8)
		}
		{
			p.SetState(124)
			p.OptionalExpression()
		}
		{
			p.SetState(125)
			p.Match(GoqParserT__5)
		}
		{
			p.SetState(126)
			p.OptionalExpression()
		}
		{
			p.SetState(127)
			p.Match(GoqParserT__9)
		}

	case 4:
		localctx = NewRecurseDescentContext(p, localctx)
		p.EnterOuterAlt(localctx, 4)
		{
			p.SetState(129)
			p.Match(GoqParserT__28)
		}

	case 5:
		localctx = NewIdentityContext(p, localctx)
		p.EnterOuterAlt(localctx, 5)
		{
			p.SetState(130)
			p.Match(GoqParserT__27)
		}

	}

	return localctx
}

// IOptionalExpressionContext is an interface to support dynamic dispatch.
type IOptionalExpressionContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsOptionalExpressionContext differentiates from other interfaces.
	IsOptionalExpressionContext()
}

type OptionalExpressionContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyOptionalExpressionContext() *OptionalExpressionContext {
	var p = new(OptionalExpressionContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = GoqParserRULE_optionalExpression
	return p
}

func (*OptionalExpressionContext) IsOptionalExpressionContext() {}

func NewOptionalExpressionContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *OptionalExpressionContext {
	var p = new(OptionalExpressionContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = GoqParserRULE_optionalExpression

	return p
}

func (s *OptionalExpressionContext) GetParser() antlr.Parser { return s.parser }

func (s *OptionalExpressionContext) Expression() IExpressionContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IExpressionContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IExpressionContext)
}

func (s *OptionalExpressionContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *OptionalExpressionContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *OptionalExpressionContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case GoqVisitor:
		return t.VisitOptionalExpression(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *GoqParser) OptionalExpression() (localctx IOptionalExpressionContext) {
	localctx = NewOptionalExpressionContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 4, GoqParserRULE_optionalExpression)
	var _la int

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	p.SetState(134)
	p.GetErrorHandler().Sync(p)
	_la = p.GetTokenStream().LA(1)

	if (((_la)&-(0x1f+1)) == 0 && ((1<<uint(_la))&((1<<GoqParserT__0)|(1<<GoqParserT__2)|(1<<GoqParserT__3)|(1<<GoqParserT__8)|(1<<GoqParserT__10)|(1<<GoqParserT__27)|(1<<GoqParserT__28)|(1<<GoqParserBOOL)|(1<<GoqParserNAME))) != 0) || _la == GoqParserSTRING || _la == GoqParserINT {
		{
			p.SetState(133)
			p.expression(0)
		}

	}

	return localctx
}

// IObjectEntryContext is an interface to support dynamic dispatch.
type IObjectEntryContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsObjectEntryContext differentiates from other interfaces.
	IsObjectEntryContext()
}

type ObjectEntryContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyObjectEntryContext() *ObjectEntryContext {
	var p = new(ObjectEntryContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = GoqParserRULE_objectEntry
	return p
}

func (*ObjectEntryContext) IsObjectEntryContext() {}

func NewObjectEntryContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *ObjectEntryContext {
	var p = new(ObjectEntryContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = GoqParserRULE_objectEntry

	return p
}

func (s *ObjectEntryContext) GetParser() antlr.Parser { return s.parser }

func (s *ObjectEntryContext) Key() IKeyContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IKeyContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IKeyContext)
}

func (s *ObjectEntryContext) Expression() IExpressionContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IExpressionContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IExpressionContext)
}

func (s *ObjectEntryContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *ObjectEntryContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *ObjectEntryContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case GoqVisitor:
		return t.VisitObjectEntry(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *GoqParser) ObjectEntry() (localctx IObjectEntryContext) {
	localctx = NewObjectEntryContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 6, GoqParserRULE_objectEntry)
	var _la int

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(136)
		p.Key()
	}
	p.SetState(139)
	p.GetErrorHandler().Sync(p)
	_la = p.GetTokenStream().LA(1)

	if _la == GoqParserT__5 {
		{
			p.SetState(137)
			p.Match(GoqParserT__5)
		}
		{
			p.SetState(138)
			p.expression(0)
		}

	}

	return localctx
}

// IKeyContext is an interface to support dynamic dispatch.
type IKeyContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsKeyContext differentiates from other interfaces.
	IsKeyContext()
}

type KeyContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyKeyContext() *KeyContext {
	var p = new(KeyContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = GoqParserRULE_key
	return p
}

func (*KeyContext) IsKeyContext() {}

func NewKeyContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *KeyContext {
	var p = new(KeyContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = GoqParserRULE_key

	return p
}

func (s *KeyContext) GetParser() antlr.Parser { return s.parser }

func (s *KeyContext) NAME() antlr.TerminalNode {
	return s.GetToken(GoqParserNAME, 0)
}

func (s *KeyContext) STRING() antlr.TerminalNode {
	return s.GetToken(GoqParserSTRING, 0)
}

func (s *KeyContext) Expression() IExpressionContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IExpressionContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IExpressionContext)
}

func (s *KeyContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *KeyContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *KeyContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case GoqVisitor:
		return t.VisitKey(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *GoqParser) Key() (localctx IKeyContext) {
	localctx = NewKeyContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 8, GoqParserRULE_key)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.SetState(147)
	p.GetErrorHandler().Sync(p)

	switch p.GetTokenStream().LA(1) {
	case GoqParserNAME:
		p.EnterOuterAlt(localctx, 1)
		{
			p.SetState(141)
			p.Match(GoqParserNAME)
		}

	case GoqParserSTRING:
		p.EnterOuterAlt(localctx, 2)
		{
			p.SetState(142)
			p.Match(GoqParserSTRING)
		}

	case GoqParserT__0:
		p.EnterOuterAlt(localctx, 3)
		{
			p.SetState(143)
			p.Match(GoqParserT__0)
		}
		{
			p.SetState(144)
			p.expression(0)
		}
		{
			p.SetState(145)
			p.Match(GoqParserT__1)
		}

	default:
		panic(antlr.NewNoViableAltException(p, nil, nil, nil, nil, nil))
	}

	return localctx
}

// IParamContext is an interface to support dynamic dispatch.
type IParamContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsParamContext differentiates from other interfaces.
	IsParamContext()
}

type ParamContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyParamContext() *ParamContext {
	var p = new(ParamContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = GoqParserRULE_param
	return p
}

func (*ParamContext) IsParamContext() {}

func NewParamContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *ParamContext {
	var p = new(ParamContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = GoqParserRULE_param

	return p
}

func (s *ParamContext) GetParser() antlr.Parser { return s.parser }

func (s *ParamContext) NAME() antlr.TerminalNode {
	return s.GetToken(GoqParserNAME, 0)
}

func (s *ParamContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *ParamContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *ParamContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case GoqVisitor:
		return t.VisitParam(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *GoqParser) Param() (localctx IParamContext) {
	localctx = NewParamContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 10, GoqParserRULE_param)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(149)
		p.Match(GoqParserNAME)
	}

	return localctx
}

func (p *GoqParser) Sempred(localctx antlr.RuleContext, ruleIndex, predIndex int) bool {
	switch ruleIndex {
	case 0:
		var t *ExpressionContext = nil
		if localctx != nil {
			t = localctx.(*ExpressionContext)
		}
		return p.Expression_Sempred(t, predIndex)

	default:
		panic("No predicate with index: " + fmt.Sprint(ruleIndex))
	}
}

func (p *GoqParser) Expression_Sempred(localctx antlr.RuleContext, predIndex int) bool {
	switch predIndex {
	case 0:
		return p.Precpred(p.GetParserRuleContext(), 7)

	case 1:
		return p.Precpred(p.GetParserRuleContext(), 6)

	case 2:
		return p.Precpred(p.GetParserRuleContext(), 5)

	case 3:
		return p.Precpred(p.GetParserRuleContext(), 4)

	case 4:
		return p.Precpred(p.GetParserRuleContext(), 3)

	case 5:
		return p.Precpred(p.GetParserRuleContext(), 2)

	case 6:
		return p.Precpred(p.GetParserRuleContext(), 15)

	case 7:
		return p.Precpred(p.GetParserRuleContext(), 14)

	case 8:
		return p.Precpred(p.GetParserRuleContext(), 1)

	default:
		panic("No predicate with index: " + fmt.Sprint(predIndex))
	}
}
