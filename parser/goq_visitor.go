// Generated from Goq.g4 by ANTLR 4.7.

package parser // Goq

import "github.com/antlr/antlr4/runtime/Go/antlr"

// A complete Visitor for a parse tree produced by GoqParser.
type GoqVisitor interface {
	antlr.ParseTreeVisitor

	// Visit a parse tree produced by GoqParser#lgOperators.
	VisitLgOperators(ctx *LgOperatorsContext) interface{}

	// Visit a parse tree produced by GoqParser#basicExpressions.
	VisitBasicExpressions(ctx *BasicExpressionsContext) interface{}

	// Visit a parse tree produced by GoqParser#def.
	VisitDef(ctx *DefContext) interface{}

	// Visit a parse tree produced by GoqParser#bool.
	VisitBool(ctx *BoolContext) interface{}

	// Visit a parse tree produced by GoqParser#string.
	VisitString(ctx *StringContext) interface{}

	// Visit a parse tree produced by GoqParser#boolOperators.
	VisitBoolOperators(ctx *BoolOperatorsContext) interface{}

	// Visit a parse tree produced by GoqParser#integer.
	VisitInteger(ctx *IntegerContext) interface{}

	// Visit a parse tree produced by GoqParser#any.
	VisitAny(ctx *AnyContext) interface{}

	// Visit a parse tree produced by GoqParser#expressions.
	VisitExpressions(ctx *ExpressionsContext) interface{}

	// Visit a parse tree produced by GoqParser#eqOperators.
	VisitEqOperators(ctx *EqOperatorsContext) interface{}

	// Visit a parse tree produced by GoqParser#comma.
	VisitComma(ctx *CommaContext) interface{}

	// Visit a parse tree produced by GoqParser#null.
	VisitNull(ctx *NullContext) interface{}

	// Visit a parse tree produced by GoqParser#array.
	VisitArray(ctx *ArrayContext) interface{}

	// Visit a parse tree produced by GoqParser#function.
	VisitFunction(ctx *FunctionContext) interface{}

	// Visit a parse tree produced by GoqParser#mulOperations.
	VisitMulOperations(ctx *MulOperationsContext) interface{}

	// Visit a parse tree produced by GoqParser#addOperations.
	VisitAddOperations(ctx *AddOperationsContext) interface{}

	// Visit a parse tree produced by GoqParser#noError.
	VisitNoError(ctx *NoErrorContext) interface{}

	// Visit a parse tree produced by GoqParser#group.
	VisitGroup(ctx *GroupContext) interface{}

	// Visit a parse tree produced by GoqParser#object.
	VisitObject(ctx *ObjectContext) interface{}

	// Visit a parse tree produced by GoqParser#objectIdentifier.
	VisitObjectIdentifier(ctx *ObjectIdentifierContext) interface{}

	// Visit a parse tree produced by GoqParser#index.
	VisitIndex(ctx *IndexContext) interface{}

	// Visit a parse tree produced by GoqParser#slice.
	VisitSlice(ctx *SliceContext) interface{}

	// Visit a parse tree produced by GoqParser#recurseDescent.
	VisitRecurseDescent(ctx *RecurseDescentContext) interface{}

	// Visit a parse tree produced by GoqParser#identity.
	VisitIdentity(ctx *IdentityContext) interface{}

	// Visit a parse tree produced by GoqParser#optionalExpression.
	VisitOptionalExpression(ctx *OptionalExpressionContext) interface{}

	// Visit a parse tree produced by GoqParser#objectEntry.
	VisitObjectEntry(ctx *ObjectEntryContext) interface{}

	// Visit a parse tree produced by GoqParser#key.
	VisitKey(ctx *KeyContext) interface{}

	// Visit a parse tree produced by GoqParser#param.
	VisitParam(ctx *ParamContext) interface{}
}
