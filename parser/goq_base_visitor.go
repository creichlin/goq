// Generated from Goq.g4 by ANTLR 4.7.

package parser // Goq

import "github.com/antlr/antlr4/runtime/Go/antlr"

type BaseGoqVisitor struct {
	*antlr.BaseParseTreeVisitor
}

func (v *BaseGoqVisitor) VisitLgOperators(ctx *LgOperatorsContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseGoqVisitor) VisitBasicExpressions(ctx *BasicExpressionsContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseGoqVisitor) VisitDef(ctx *DefContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseGoqVisitor) VisitBool(ctx *BoolContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseGoqVisitor) VisitString(ctx *StringContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseGoqVisitor) VisitBoolOperators(ctx *BoolOperatorsContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseGoqVisitor) VisitInteger(ctx *IntegerContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseGoqVisitor) VisitAny(ctx *AnyContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseGoqVisitor) VisitExpressions(ctx *ExpressionsContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseGoqVisitor) VisitEqOperators(ctx *EqOperatorsContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseGoqVisitor) VisitComma(ctx *CommaContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseGoqVisitor) VisitNull(ctx *NullContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseGoqVisitor) VisitArray(ctx *ArrayContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseGoqVisitor) VisitFunction(ctx *FunctionContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseGoqVisitor) VisitMulOperations(ctx *MulOperationsContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseGoqVisitor) VisitAddOperations(ctx *AddOperationsContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseGoqVisitor) VisitNoError(ctx *NoErrorContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseGoqVisitor) VisitGroup(ctx *GroupContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseGoqVisitor) VisitObject(ctx *ObjectContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseGoqVisitor) VisitObjectIdentifier(ctx *ObjectIdentifierContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseGoqVisitor) VisitIndex(ctx *IndexContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseGoqVisitor) VisitSlice(ctx *SliceContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseGoqVisitor) VisitRecurseDescent(ctx *RecurseDescentContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseGoqVisitor) VisitIdentity(ctx *IdentityContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseGoqVisitor) VisitOptionalExpression(ctx *OptionalExpressionContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseGoqVisitor) VisitObjectEntry(ctx *ObjectEntryContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseGoqVisitor) VisitKey(ctx *KeyContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseGoqVisitor) VisitParam(ctx *ParamContext) interface{} {
	return v.VisitChildren(ctx)
}
