goq
===

goq is a query language for go to query composite objects in go, mainly maps and slices.
it can also be used to query json or yaml files which easily can be read from go.
 
goq syntax is a subset of https://stedolan.github.io/jq i try to keep it as close as
possible

it's pretty much a prototype and i will not continue working on it

progress
--------
 - parser using antlr4
 - transformation to ast
 - about half of jq tests run
