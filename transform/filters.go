package transform

import (
	"gitlab.com/creichlin/goq/ast"
	"gitlab.com/creichlin/goq/parser"
)

func (s *TransformVisitor) VisitAny(ctx *parser.AnyContext) interface{} {
	return &ast.Any{
		Of: ctx.Expression().Accept(s).(ast.Element),
	}
}

func (s *TransformVisitor) VisitIdentity(ctx *parser.IdentityContext) interface{} {
	return &ast.ID{}
}

func (s *TransformVisitor) VisitRecurseDescent(ctx *parser.RecurseDescentContext) interface{} {
	return &ast.Recurse{
		Filter: &ast.NoError{Element: &ast.Any{Of: &ast.ID{}}},
		Condition: &ast.BinaryOperation{
			Operation: "!=",
			Left:      &ast.ID{},
			Right:     ast.NULL,
		},
	}
}

func (s *TransformVisitor) VisitIndex(ctx *parser.IndexContext) interface{} {
	return &ast.Index{
		Name: ctx.Expression().Accept(s).(ast.Element),
	}
}

func (s *TransformVisitor) VisitSlice(ctx *parser.SliceContext) interface{} {
	return &ast.Slice{
		From: ctx.OptionalExpression(0).Accept(s).(ast.Element),
		To:   ctx.OptionalExpression(1).Accept(s).(ast.Element),
		Step: ast.NULL,
	}
}

func (s *TransformVisitor) VisitObjectIdentifier(ctx *parser.ObjectIdentifierContext) interface{} {
	return &ast.Index{
		Name: ast.NewString(ctx.NAME().GetText()),
	}
}

func (s *TransformVisitor) VisitOptionalExpression(ctx *parser.OptionalExpressionContext) interface{} {
	if ctx.Expression() == nil {
		return ast.NULL
	}
	return ctx.Expression().Accept(s)
}

func (s *TransformVisitor) VisitFunction(ctx *parser.FunctionContext) interface{} {
	f := &ast.Function{Name: ctx.NAME().GetText()}
	for _, exp := range ctx.AllExpression() {
		f.Parameters = append(f.Parameters, exp.Accept(s).(ast.Element))
	}
	return f
}

func (s *TransformVisitor) VisitNoError(ctx *parser.NoErrorContext) interface{} {
	return &ast.NoError{Element: ctx.Expression().Accept(s).(ast.Element)}
}

func (s *TransformVisitor) VisitDef(ctx *parser.DefContext) interface{} {
	params := []*ast.Param{}
	for _, p := range ctx.AllParam() {
		params = append(params, p.Accept(s).(*ast.Param))
	}

	return &ast.Def{
		Name:   ctx.NAME().GetText(),
		Code:   ctx.Expression().Accept(s).(ast.Element),
		Params: params,
	}
}

func (s *TransformVisitor) VisitParam(ctx *parser.ParamContext) interface{} {
	return &ast.Param{
		Name: ctx.NAME().GetText(),
	}
}
