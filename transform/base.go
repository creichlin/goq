package transform

import (
	"gitlab.com/creichlin/goq/ast"
	"gitlab.com/creichlin/goq/parser"
)

type TransformVisitor struct {
	*parser.BaseGoqVisitor
}

func NewTransformVisitor() parser.GoqVisitor { // return interface to make sure it's implemented
	return &TransformVisitor{
		BaseGoqVisitor: &parser.BaseGoqVisitor{},
	}
}

func (s *TransformVisitor) VisitExpressions(ctx *parser.ExpressionsContext) interface{} {
	filters := &ast.Expressions{}

	for _, e := range ctx.AllExpression() {
		if el, found := e.Accept(s).(ast.Element); found {
			filters.Filters = append(filters.Filters, el)
		} else {
			//panic(fmt.Sprintf("could not process '%v' child returned %T", e.GetText(), e.Accept(s)))
		}
	}

	return filters
}

func (s *TransformVisitor) VisitBasicExpressions(ctx *parser.BasicExpressionsContext) interface{} {
	filters := &ast.Expressions{}

	for _, e := range ctx.AllBasicExpression() {
		if el, found := e.Accept(s).(ast.Element); found {
			filters.Filters = append(filters.Filters, el)
		} else {
			//panic(fmt.Sprintf("could not process '%v' child returned %T", e.GetText(), e.Accept(s)))
		}
	}
	return filters
}

func (s *TransformVisitor) VisitGroup(ctx *parser.GroupContext) interface{} {
	return &ast.Block{ctx.Expression().Accept(s).(ast.Element)}
}
