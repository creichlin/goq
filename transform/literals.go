package transform

import (
	"strconv"

	"gitlab.com/creichlin/goq/ast"
	"gitlab.com/creichlin/goq/parser"
)

func (s *TransformVisitor) VisitInteger(ctx *parser.IntegerContext) interface{} {
	i, _ := strconv.Atoi(ctx.GetText())
	return &ast.Number{Value: float64(i)}
}

func (s *TransformVisitor) VisitString(ctx *parser.StringContext) interface{} {
	return ast.NewEscapedString(ctx.GetText())
}

func (s *TransformVisitor) VisitNull(ctx *parser.NullContext) interface{} {
	return &ast.Null{}
}

func (s *TransformVisitor) VisitArray(ctx *parser.ArrayContext) interface{} {
	return &ast.Array{
		Elements: ctx.Expression().Accept(s).(ast.Element),
	}
}

func (s *TransformVisitor) VisitObject(ctx *parser.ObjectContext) interface{} {
	obj := &ast.Object{}

	for _, entry := range ctx.AllObjectEntry() {
		obj.Entries = append(obj.Entries, entry.Accept(s).(*ast.Entry))
	}

	return obj
}

func (s *TransformVisitor) VisitObjectEntry(ctx *parser.ObjectEntryContext) interface{} {
	entry := &ast.Entry{
		Key: ctx.Key().Accept(s).(ast.Element),
	}
	if ctx.Expression() != nil {
		entry.Value = ctx.Expression().Accept(s).(ast.Element)
	} else {
		entry.Value = &ast.Index{Name: entry.Key}
	}
	return entry
}

func (s *TransformVisitor) VisitKey(ctx *parser.KeyContext) interface{} {
	if ctx.NAME() != nil {
		return ast.NewString(ctx.NAME().GetText())
	} else if ctx.STRING() != nil {
		return ast.NewEscapedString(ctx.STRING().GetText())
	} else {
		return ctx.Expression().Accept(s)
	}
}

func (s *TransformVisitor) VisitBool(ctx *parser.BoolContext) interface{} {
	if ctx.GetText() == "true" {
		return ast.Bool(true)
	}
	return ast.Bool(false)
}
