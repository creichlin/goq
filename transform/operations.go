package transform

import (
	"gitlab.com/creichlin/goq/ast"
	"gitlab.com/creichlin/goq/parser"
)

func (s *TransformVisitor) VisitAddOperations(ctx *parser.AddOperationsContext) interface{} {
	return &ast.BinaryOperation{
		Operation: ctx.GetOp().GetText(),
		Left:      ctx.Expression(0).Accept(s).(ast.Element),
		Right:     ctx.Expression(1).Accept(s).(ast.Element),
	}
}

func (s *TransformVisitor) VisitMulOperations(ctx *parser.MulOperationsContext) interface{} {
	return &ast.BinaryOperation{
		Operation: ctx.GetOp().GetText(),
		Left:      ctx.Expression(0).Accept(s).(ast.Element),
		Right:     ctx.Expression(1).Accept(s).(ast.Element),
	}
}

func (s *TransformVisitor) VisitComma(ctx *parser.CommaContext) interface{} {
	return &ast.Comma{
		Left:  ctx.Expression(0).Accept(s).(ast.Element),
		Right: ctx.Expression(1).Accept(s).(ast.Element),
	}
}

func (s *TransformVisitor) VisitLgOperators(ctx *parser.LgOperatorsContext) interface{} {
	return &ast.BinaryOperation{
		Operation: ctx.GetOp().GetText(),
		Left:      ctx.Expression(0).Accept(s).(ast.Element),
		Right:     ctx.Expression(1).Accept(s).(ast.Element),
	}
}

func (s *TransformVisitor) VisitBoolOperators(ctx *parser.BoolOperatorsContext) interface{} {
	return &ast.BinaryOperation{
		Operation: ctx.GetOp().GetText(),
		Left:      ctx.Expression(0).Accept(s).(ast.Element),
		Right:     ctx.Expression(1).Accept(s).(ast.Element),
	}
}

func (s *TransformVisitor) VisitEqOperators(ctx *parser.EqOperatorsContext) interface{} {
	return &ast.BinaryOperation{
		Operation: ctx.GetOp().GetText(),
		Left:      ctx.Expression(0).Accept(s).(ast.Element),
		Right:     ctx.Expression(1).Accept(s).(ast.Element),
	}
}
