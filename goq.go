package goq

import (
	"errors"
	"strconv"

	"github.com/antlr/antlr4/runtime/Go/antlr"
	"github.com/creichlin/gutil"
	"gitlab.com/creichlin/goq/ast"
	"gitlab.com/creichlin/goq/evaluate"
	"gitlab.com/creichlin/goq/parser"
	"gitlab.com/creichlin/goq/transform"
)

const builtins = `
def map(f): [.[] | f];
`

func Evaluate(a ast.Element, data interface{}) (*evaluate.Data, error) {
	eval := evaluate.NewEval()

	b, err := ReadFromString(builtins)
	if err != nil {
		return nil, err
	}

	_, err = eval.Eval(b, evaluate.NewEmptyData())
	if err != nil {
		return nil, err
	}

	return eval.Eval(a, evaluate.NewSingleData(data))
}

func ReadFromString(input string) (ast.Element, error) {
	inputStream := antlr.NewInputStream(input)
	errs := &errorCollectorListener{errors: &gutil.ErrorCollector{}}

	lexer := parser.NewGoqLexer(inputStream)
	lexer.RemoveErrorListeners()
	lexer.AddErrorListener(errs)

	stream := antlr.NewCommonTokenStream(lexer, 0)
	p := parser.NewGoqParser(stream)
	p.RemoveErrorListeners()
	p.AddErrorListener(errs)
	p.BuildParseTrees = true
	tree := p.Expression()

	if errs.errors.Has() {
		return nil, errs.errors.ThisOrNil()
	}

	st := tree.Accept(transform.NewTransformVisitor())
	return st.(ast.Element), nil
}

type errorCollectorListener struct {
	errors *gutil.ErrorCollector
}

func (c *errorCollectorListener) SyntaxError(recognizer antlr.Recognizer, offendingSymbol interface{},
	line, column int, msg string, e antlr.RecognitionException) {
	c.errors.Add(errors.New("line " + strconv.Itoa(line) + ":" + strconv.Itoa(column) + " " + msg))
}

func (c *errorCollectorListener) ReportAmbiguity(recognizer antlr.Parser, dfa *antlr.DFA, startIndex, stopIndex int, exact bool, ambigAlts *antlr.BitSet, configs antlr.ATNConfigSet) {
	// c.errors.Add(errors.New(dfa.ToLexerString()))
}

func (c *errorCollectorListener) ReportAttemptingFullContext(recognizer antlr.Parser, dfa *antlr.DFA, startIndex, stopIndex int, conflictingAlts *antlr.BitSet, configs antlr.ATNConfigSet) {
	// c.errors.Add(errors.New(dfa.ToLexerString()))
}

func (c *errorCollectorListener) ReportContextSensitivity(recognizer antlr.Parser, dfa *antlr.DFA, startIndex, stopIndex, prediction int, configs antlr.ATNConfigSet) {
	//c.errors.Add(errors.New(dfa.ToLexerString()))
}
